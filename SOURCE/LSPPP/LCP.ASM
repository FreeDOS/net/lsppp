;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
include ppp.asi
include internet.asi
;
; note that I never send term-request, but instead simply drop the carrier
; mean, but effective...
;
; also I'm not sure when configuration options take effect
;
; finally the configuration stuff is hard-coded, if you add configuration
; options you will have to add them in several places
;
;
; flags for which options we can send in a CONFREQ packet
;
CF_PROTCMP = 1
CF_ADRCMP = 2
CF_CONTMAP = 4
CF_MRU = 8

CGROUP group TSR,CONFIG
	assume cs:cgroup,ds:cgroup
	public LCP_handler, LCP_init, lcpdown, LCP_struc, peermru
        public defcontmap, SendEchoReq, receivemru
	extrn	tlu : PROC, tld : PROC
	extrn xltabxmit : byte, xltabrcv : PROC, listedprothandler : PROC
	extrn authenticated : byte, iphandle : proc
	extrn PAP_INIT : PROC,PAP_HANDLER : PROC
	extrn sm_init : PROC, sm_handler : PROC
	extrn packetflags : word, confbuf : byte
	extrn IPCP_struc : statemachine
        extrn gettime :PROC, stdpacket : PROC
	extrn	confbuf : byte, StartXmit : PROC
TSR segment word public 'CODE'
defconf		db	LCPC_PROTCOMP,2,LCPC_ADRCOMP,2
magicnum	db	LCPC_MAGIC,6,0,0,0,0
defcontmap	db	LCPC_CONTMAP,6,0,0,0,0
		db	LCPC_MRU,4
receivemru      dw      1500
LCP_struc	statemachine	<CF_PROTCMP + CF_ADRCMP + CF_CONTMAP,0,0,0,0,DEFAULT_COUNT,0,DEFAULT_TIME*18 + DEFAULT_TIME/5,0,LCP_PROTOCOL,lcpreneg,lcpreq,lcpaccept,lcpconfack,lcpverify,lcpnoreject,lcpdown,lcpup>
					; to send in conf reqs
peermru		dw	1500
echoreq         db      LCP_ECHOREQ,0,0,8
                dd      0
seed	dd	0
mulval	dd	15a4e35h
;
; decide on a magic number for us...
;
redomagic PROC
	push	dx
	push	cx
	push	bx
	push	ax
	call	gettime
	mov	word ptr [seed],ax
	xchg	al,ah
	mov	word ptr [seed+2],ax
;
	
	mov	ax,word ptr [seed]	; this is a partial 32x32 multiply
	mul	word ptr [mulval]	; which discards the upper 32-bits
	mov	cx,ax			; of the result
	mov	bx,dx
	mov	ax,word ptr [seed + 2]
	mul	word ptr [mulval]
	add	bx,ax
	mov	ax,word ptr [seed]
	mul	word ptr [mulval+2]
	add	bx,ax
	add	cx,1
	adc	bx,0
	mov	word ptr [seed],cx
	mov	word ptr [seed + 2],bx
	mov	word ptr [magicnum+2],cx
	mov	word ptr [magicnum+4],bx
	pop	ax
	pop	bx
	pop	cx
	pop	dx
	ret
redomagic ENDP
;
; renegotiate the cont map, or delete address/protocol compression
; if they didn't like that...
;
lcpreneg PROC
	mov	al,byte ptr [si]
	cmp	al,LCPC_MAGIC	; they didn't like our magic?
	jnz	scrnmc
	call	redomagic	; yep, try again with a new one...
				; we are supporting magic numbers for the
				; other end's sake, but if we are looped back
				; we are locally going to thrash forever...
	ret
scrnmc:
	cmp	al,LCPC_PROTCOMP	; protcmp?
	jnz	scrnpc
	and	[bx].sm_flags,NOT CF_PROTCMP	; yep, don't send again
	ret

scrnpc:
	cmp	al,LCPC_ADRCOMP		; adrcmp?
	jnz	scrnac
	and	[bx].sm_flags,NOT CF_ADRCMP	; yep, don't send it again
	ret
scrnac:
	cmp	al,LCPC_CONTMAP		; CONT map?
	jnz	scrncm
	test	dl,dl			; is it reject?
	jz	contreneg		; no renegotiate
	and	[bx].sm_flags,NOT CF_CONTMAP	; yes- don't send it again
scrncm:
	cmp	al,LCPC_MRU
	jnz	scrnmru
	and	[bx].sm_flags,NOT CF_MRU
scrnmru:
	ret
contreneg:
	mov	ax,[si+2]
	mov	word ptr [defcontmap+2],ax
	mov	ax,[si+4]
	mov	word ptr [defcontmap+4],ax
	ret
lcpreneg ENDP
;
; we send our current confreq or timeout has occurred so resend
;
lcpreq	PROC
	sub	dx,dx
	mov	si,offset cgroup:defconf	; get def configuration
	lodsw
	test	[bx].sm_flags,CF_PROTCMP	; see if doing protocol comp
	jz	noprot
	stosw				; yes do it
	add	dx,2
noprot:
	lodsw
	test	[bx].sm_flags,CF_ADRCMP	; see if doing adrcmp
	jz	noadr			;
	stosw				; yes do it
	add	dx,2
noadr:
	movsw				; move magic
	movsw
	movsw
	add	dx,6

	test	[bx].sm_flags,CF_CONTMAP	; see if doing contmap
	jz	nocontmap
	movsw				; yes do it
	movsw
	movsw
	add	dx,6
nocontmap:
	test	[bx].sm_flags,CF_MRU
	jz	nomru
	movsw
	movsw
	add	dx,4
nomru:
	mov	ax,dx
	ret
lcpreq	ENDP
;
; routine to process an entire cont map
;
contmap PROC
	push	si		; save regs
	push	di
	add	si,2		; point to the contmap
	mov	di,dx		; di = position to save to
	lodsw			; have to adjust for endianness on the move
	xchg	al,ah
	mov	dx,ax
	lodsw
	xchg	al,ah
	or	ax,[di]
	stosw
	xchg	ax,dx
	or	ax,[di]
	stosw
	pop	di
	pop	si
	ret
contmap ENDP
;
; they like our conf
;
lcpconfack PROC
	cmp	BYTE PTR [si],LCPC_PROTCOMP	; is it protcmp?
	jnz	ocac
	or	[packetflags],PF_XMITPROTOCOMPRESS ; yep, do it from now on
ocac:
	cmp	BYTE PTR [si],LCPC_ADRCOMP	; is it adrcomp?
	jnz	ocm
	or	[packetflags],PF_XMITADRCOMPRESS ; yes, do it from now on
ocm:
	cmp	BYTE PTR [si],LCPC_CONTMAP	; is it contmap?
	jnz	ocx
	mov	dx,offset cgroup:xltabrcv
	call	contmap			; yes, load the cont map
ocx:
	ret
lcpconfack ENDP
;
; we like theirs
;
; we don't care about what they decide about compression &
; cont maps, we auto-detect all that stuff based on the incoming
; stream.  So all we need to do is check if they want authentication
; and it has already been renegotiated if necessary
;
lcpaccept PROC
	
	cmp	BYTE PTR [si],LCPC_CONTMAP	; is it contmap?
	jnz 	ipaauth
	mov	dx,offset cgroup:xltabxmit
	call	contmap			; yes, load the cont map
	ret
ipaauth:
	cmp	BYTE PTR [si],LCPC_AUTH	; is it auth?
	jnz	ipamru
	and	[authenticated],NOT AU_AUTH ; yes, mark us un-authenticated
	cmp	word ptr [si+2],23c2H	; chap?
	jnz	ipzx
     	or	[authenticated],AU_CHAP	; they better not have switched away from MD5!!!
ipzx:
	ret
ipamru:
	cmp	BYTE PTR [si],LCPC_MRU ; is it mru ?
	jnz	ipax
	mov	ax,[si+2]		; yes, get it
	xchg	al,ah			; make little endian
	mov	[peermru],ax		; save it
ipax:
	ret
lcpaccept ENDP	
;
; Check if a particular conf entry is valid or not,
; ans switch to PAP protocol in case they specified something else
;
lcpverify PROC
	lahf
	cmp	byte ptr [si],LCPC_MRU
	jnz	lv2
	push	ax
	mov	ax,[si+2]
	xchg	al,ah
        cmp     ax,[peermru]
	pop	ax
        jle     vce2
	sahf
	jnc	mrunofix
	push	ax
        mov     ax,[peermru]
    	mov	[si+2],ah
	mov	[si+3],al
	pop	ax
mrunofix:
	stc
	ret
vce2:
	clc
	ret
lv2:
	cmp	byte ptr [si],LCPC_AUTH	; if it is auth make sure they asked for PAP
	jz	vcn
	cmp	byte ptr [si],LCPC_CONTMAP ; else accept any of these
	jz	vce
	cmp	byte ptr [si],LCPC_PROTCOMP
	jz	vce
	cmp	byte ptr [si],LCPC_ADRCOMP
	jz	vce
	cmp	byte ptr [si],LCPC_MAGIC
	jnz	vcbm
	mov	ax,[si+2]	; magic of zero is disallowed, nak it
	or	ax,[si+4]
	jz	vcmex
	mov	ax,[si+2]	; else if we get our own magic in a confreg nak it
	cmp	ax,word ptr [magicnum+2]
	jnz	vcmne
	mov	ax,[si+4]
	cmp	ax,word ptr [magicnum+4]
	jnz	vcmne
vcmex:
	stc
	ret
vcmne:
;	mov	ax,word ptr [magicnum+2]
;	mov	[si+2],ax
;	mov	ax,word ptr [magicnum+4]
;	mov	[si+4],ax
	clc
	ret
vcbm:
	mov	[confbuf],LCP_CONFREJ
	stc
vce:
	ret
vcn:
	cmp	word ptr [si+2],23c0h ; AUTH, did they specify PAP
	jz	vce
ifndef NOCHAP
	cmp	word ptr [si+2],23C2h ; specify chap?
	jnz	nofix			; no, get out
	cmp	byte ptr [si+4],5	; md5?
	jz	vce			; yes, accept
	or	[authenticated],AU_UNKNOWN ; tell the connect routine we can't do this
	ret
endif
nofix:
	stc
	ret
lcpverify ENDP
lcpnoreject	PROC
	mov	al,byte ptr [si]	; figure out which options are
	or	al,al			; rejectable (0,4,6, >8)
	jz	rej
	cmp	al,4
	jz	rej
	cmp	al,6
	jz	rej
	cmp	al,9
	jae	rej
	xor	al,al
	ret
rej:
	or	al,1
	ret
lcpnoreject	ENDP
lcpdown PROC
	mov	[authenticated],0
;        mov     [LCP_struc].sm_state, ST_INITIAL
	call	tld
	ret
lcpdown ENDP
lcpup	PROC
	call	PAP_init
	call	tlu
	ret
lcpup	ENDP
SendEchoReq proc
        mov     ax,word ptr [magicnum+2]
        mov     word ptr [echoreq + 4],ax
        mov     ax,word ptr [magicnum+4]
        mov     word ptr [echoreq + 6],ax
        mov     si,offset cgroup:echoreq
        mov     bx,offset cgroup:lcp_struc
        jmp     stdpacket
SendEchoReq endp
;
; when we receive something in PPP it comes here
;
LCP_handler PROC
	mov	bx,offset cgroup:IPCP_struc		; first see if IPCP
	cmp	[bx].sm_state,ST_OPENED		; is opened
	jnz	nolist				; no, these protocols not known
	call	iphandle			; else handle IP packets
	jnc	handled
	call	listedprothandler		; if not handle other unknown packets
	jnc	handled
nolist:
	mov	bx,offset cgroup:LCP_struc		; now see if is LCP
	call	sm_handler
	jnc	handled
	mov	bx,offset cgroup:LCP_struc		; now see if is LCP
	cmp	[bx].sm_state,ST_OPENED		; if not we must have LCP opened
	jnz	badprot
	call	PAP_handler			; before we can do PAP
	jnc	handled
	mov	bx,offset cgroup:IPCP_struc		; or IPCP
	call	sm_handler
	jc	badprot
handled:
	clc
	ret
badprot:
	cmp	ax,IP_PROTOCOL			; never reject IP or ARP
	jz	handled
	cmp	ax,ELT_ARP
	jz	handled
	mov	dx,ax
	mov	di,offset cgroup:confbuf	; store a reject msg, ID=0
	push	di
	mov	al,LCP_PROTREJ
	mov	ah,[LCP_struc].sm_index		; need to add an index field
	inc	[LCP_struc].sm_index
	stosw
	cmp	cx,MRU-8			; see if two big?
	jc	sprok
	mov	cx,MRU-8			; yes, max it out

sprok:
	mov	ax,cx			; now copy their packet
	inc	cx
	inc	cx
	push	ax
	xchg	al,ah
	stosw				; store the count field
; fixme - should add address bytes in .
	mov	ax,dx
	xchg	al,ah
	stosw
	rep	movsb
	pop	cx
	add	cx,2
	pop	si
	mov	ax,LCP_PROTOCOL
	jmp	startxmit

LCP_HANDLER ENDP
LCP_INIT PROC
	call	redomagic
	mov	bx,offset cgroup:LCP_struc
        mov     [bx].sm_flags,CF_ADRCMP + CF_PROTCMP + CF_CONTMAP + CF_MRU
	jmp	sm_init
LCP_INIT ENDP
TSR	ENDS
CONFIG	segment word public 'CODE'
CONFIG	ENDS
	end