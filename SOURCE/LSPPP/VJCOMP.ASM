;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
;TESTVJ = 1
ifdef TESTVJ
	.model small
	.stack
endif

include ppp.asi
include internet.asi
;
; this code derived more or less directly from the code in RFC1144
;
; generic constants
;
MAX_HDR = 128     ;/* max TCP+IP hdr length (by protocol def) */
;
; VJ change flags
;
NEW_C  =40h
NEW_I  =20h
TCP_PUSH_BIT =10h
NEW_S  =08h
NEW_A  =04h
NEW_W  =02h
NEW_U  =01h

;
; same as definition in IPCP.ASM
;
IF_XMITCOMP = 4
 
;  /* reserved, special-case values of above */
SPECIAL_I = NEW_S+NEW_W+NEW_U        ;/* echoed interactive traffic */
SPECIAL_D = NEW_S+NEW_A+NEW_W+NEW_U  ;/* unidirectional data */
SPECIALS_MASK = NEW_S+NEW_A+NEW_W+NEW_U

;
; compression state structure
;
cstate struc
cs_next	dw	?	; pointer to most recently used tstate
cs_hlen	dw	?
cs_id	db	?
cs_fill	db	?
cs_ip	db	MAX_HDR DUP (0)	; ip struct
cstate ends

;
; serial line compression data
;
slcompdat struc
last_cs	dw	?	; pointer to last xmit received state
last_recv	db	?
last_xmit	db	?
sflags		db	?
tstate	cstate MAX_VJ_STATES DUP (?)
rstate  cstate MAX_VJ_STATES DUP (?)
slcompdat ends
;
; flags for the above sflags field
;
SLF_TOSS = 1
CGROUP group TSR,CONFIG
	assume cs:cgroup,ds:cgroup

	public vjcompress,vjinit, vjerror, iphandle
ifndef NOVJ
        public xmitvjcompid,xmitvjcomp,xmitvjcompstates, 
        public vjstats, rcvvjcompstates, fillvjstats
endif
	extrn	IPCP_struc : statemachine, listedprothandler : PROC
TSR segment word public 'CODE'
ifndef NOVJ
sldat	slcompdat <>
vjstats slcompress <>
cssize	dw	size	cstate
tchgd	db	0 			; change flags, xmit
rchgd	db	0			; change flags, rcv
newhead	db	16 DUP (?)		; new header temp for compression
deltaA	dw	?			; delta ACK for compression
deltaS	dw	?			; delta SEQ for compression
xmitvjcompid	db	0		; TRUE if CID may be compressed
xmitvjcomp	db	0		; TRUE if we can do VJ comp xmitting
xmitvjcompstates db	16		; number of states for sending
rcvvjcompstates db      16              ; number of states for receiving
endif
;
; come here if we get an invalid packet receive
; don't worry about bad packets before the line comes up
; because the entire struct gets cleared when the line comes up
;
vjerror PROC
ifndef NOVJ
        add     word ptr [vjstats].sls_i_error,1
        adc     word ptr [vjstats+2].sls_i_error,0
	OR	[sldat].sflags,SLF_TOSS
endif
	ret
vjerror ENDP
ifndef NOVJ
;
; Encode something
;
; bp = pointer to encode buf, 
; ax = data to encode in intel order
;
encodez PROC
	xchg	bp,di
	test	ax,ax		; is zero?
	jz	full		; yes, full encoding
	jmp	short encodex
encodez ENDP
encode PROC
	xchg	bp,di
encode ENDP
encodex PROC
	test	ah,ah		; else is a single byte?
	jz	e1b		; yep, one byte enc
full:
	push	ax		; full encoding
	sub	al,al		; put out a leading zero
	stosb			;
	pop	ax
	xchg	al,ah		; network byte order
	stosw			; and save it
	xchg	bp,di
	ret
e1b:
	stosb
	xchg	bp,di
	ret
encodex ENDP
;
; note there is a bug in this... if IP header sizes
; differ then it will return 'different header'
; while this is slightly different from the way VJ wrote it,
; his has the same problem
;
samehdr PROC
	mov	ax,word ptr [si].ip_source		; compare sources
	cmp	ax,word ptr [di].cs_ip.ip_source
	jnz	shx				; exit if nz
	mov	ax,word ptr [si+2].ip_source		; compare sources
	cmp	ax,word ptr [di+2].cs_ip.ip_source
	jnz	shx				; exit if nz
	mov	ax,word ptr [si].ip_dest       		; compare dests
	cmp	ax,WORD ptr [di].cs_ip.ip_dest
	jnz	shx       			; exit if nz
	mov	ax,word ptr [si+2].ip_dest       		; compare dests
	cmp	ax,WORD ptr [di+2].cs_ip.ip_dest
	jnz	shx       			; exit if nz
	push	di
	lea	di,[di].cs_ip
        mov     al,[di].ip_version
        and     ax,0fh
        add     ax,ax
        add     ax,ax
        add     di,ax
	mov	ax,[si+bx].tcp_source
        cmp     ax,[di].tcp_source
	jnz	shx2
	mov	ax,[si+bx].tcp_dest
        cmp     ax,[di].tcp_dest

shx2:
	pop	di
shx:
	ret
samehdr ENDP
getlen PROC
	mov	ah,[si+bx].tcp_offs
	mov	cl,12
	shr	ax,cl
	mov	cl,[si].ip_version
	and	cx,0fh
	add	cx,ax
	ret
getlen ENDP
longsub PROC
	mov	ax,word ptr [si+bx+2]
	mov	dx,word ptr [si+bx]
	xchg	al,ah
	xchg	dl,dh
	sub	al,byte ptr [di+bx+3]
	sbb	ah,byte ptr [di+bx+2]
	sbb	dl,byte ptr [di+bx+1]
	sbb	dh,byte ptr [di+bx]
	ret
longsub ENDP
endif
vjcompress PROC
	cmp	dx,IP_PROTOCOL
	jz	vjnx
vjzx:  	clc
	ret
vjzntcp:
ifndef NOVJ
        add     word ptr [vjstats].sls_o_nontcp,1
        adc     word ptr [vjstats+2].sls_o_nontcp,0
endif
        clc
        ret
vjnx:
        mov     cx,[si].ip_length       ; get rid of padding at this point
        xchg    cl,ch
ifdef NOVJ
	ret
else
	test	[xmitvjcomp],0ffh
	jz	vjzx
	cmp	[si].ip_prot,IPP_TCP
        jnz     vjzntcp
        add     word ptr [vjstats].sls_o_tcp,1
        adc     word ptr [vjstats+2].sls_o_tcp,0
	mov	ax,[si].ip_offset
	and	ax, 0ff3fh	; remember internet byte order
	jnz	vjzx
	cmp	cx,40
	jb	vjzx
	mov	bl,[si].ip_version	; header length field (dwords)
	and	bx,0fh
	add	bx,bx
	add	bx,bx
	mov	al,[si+bx].tcp_flags
	and	al,TH_SYN + TH_FIN + TH_RST + TH_ACK
	cmp	al,TH_ACK
	jnz	vjzx
ifdef XXXXX
        push    cx
        push    si
        mov     ax,cx
        extrn   logvv : PROC
        call    logvv
        pop     si
        pop     cx
endif
;
; at this point we are able to do compression
; find a comp struct
;
	push	cx
	call	getlen		; CX = header len in DWORDS
	mov	di,[sldat].last_cs
	mov	dx,di
	mov	di,[di].cs_next
	call	samehdr
	jnz	cfind
	jmp	fnd3		; already at head, don't reorg
cfind:
        add     word ptr [vjstats].sls_o_searches,1
        adc     word ptr [vjstats+2].sls_o_searches,0
cfind2:
	mov	bp,di
	mov	di,[di].cs_next
	call	samehdr
	jz	cfound
	cmp	di,dx
        jnz     cfind2
; not found, move last to forefront and reuse
	mov	[sldat].last_cs,bp
        add     word ptr [vjstats].sls_o_misses,1
        adc     word ptr [vjstats+2].sls_o_misses,0
        jmp     uncompressed
ucg:
        sub     di,cs_ip
        jmp     uncompressed
cfound:
	cmp	di,dx
	jnz	fnd2
	mov	[sldat].last_cs,bp ; was the previous in the link,
				   ; simply update the first pointer
	jmp	fnd3
;
; found, move to forefront
;
fnd2:
	mov	ax,[di].cs_next
	mov	ds:[bp].cs_next,ax
	xchg	bx,dx
	mov	ax,[bx].cs_next
	mov	[di].cs_next,ax
	mov	[bx].cs_next,di
	xchg	bx,dx
fnd3:
;
; now verify that only things we wanted to change changed
;
	lea	di,[di].cs_ip
	mov	ax,word ptr [si].ip_version ; version and head length & TOS
	cmp	ax,word ptr [di].ip_version
	jnz	ucg
	mov	ax,word ptr [si].ip_ttl		; ttl (& protocol)
	cmp	ax,word ptr [di].ip_ttl
	jnz	ucg
	mov	ax,word ptr [si].ip_offset	; flags (offset cgroup:= 0)
	cmp	ax,word ptr [di].ip_offset
	jnz	ucg
	mov	al,[si+bx].tcp_offs
	cmp	al,[di+bx].tcp_offs
	jnz	ucg
	mov	al,[si].ip_version		; ip header options
	and	al,15
	cmp	al,5
	jbe	noipopt
	push	si
	push	di
	push	cx
	sub	ah,ah
	add	ax,ax
	mov	cx,ax
	add	ax,ax
	add	si,ax
	add	di,ax
	sub	cx,10
	repe	cmpsw
	pop	cx
	pop	di
	pop	si
ucg2:	jnz	ucg
noipopt:
	mov	al,[si+bx].tcp_offs		; tcp header options
	cmp	al,50h          		; length is in upper nibble
	jbe	notcpopt
	push	si
	push	di
	push	cx
	sub	ah,ah
	mov	cl,4
	shr	al,cl
	add	ax,ax
	mov	cx,ax
	add	ax,ax
	lea	si,[si+bx]
	lea	di,[di+bx]
	add	si,ax
	add	di,ax
	sub	cx,10
	repe	cmpsw
	pop	cx
	pop	di
	pop	si
     	jnz	ucg2
notcpopt:
	mov	[tchgd],0
	mov	bp,offset cgroup:newhead	; bp will hold header pointer
	test	[si +bx].tcp_flags, TH_URG
	jz	nurg
	mov	ax,[si+bx].tcp_urp
	xchg	al,ah
	call	encodez
	or	[tchgd],NEW_U
	jmp	nurp
nurg:
	mov	ax,[si+bx].tcp_urp
	cmp	ax,[di+bx].tcp_urp
	jz	nurp
ucg3:   
        sub     di,cs_ip
        jmp     uncompressed
nurp:
	mov	ax,[si+bx].tcp_wnd
	mov	dx,[di+bx].tcp_wnd
	xchg	al,ah
	xchg	dl,dh
	sub	ax,dx
	jz	nwin
	call	encode
	or	[tchgd],NEW_W
nwin:
	add	bx,tcp_ack
	call	longsub
	sub	bx,tcp_ack
	or	dx,dx
	jnz	ucg3
	mov	[deltaA],ax
     	or	ax,ax
	jz	nack
	call	encode
	or	[tchgd],NEW_A
nack:
	add	bx,tcp_seq
	call	longsub
	sub	bx,tcp_seq
	or	dx,dx
	jnz	ucg3
	mov	[deltaS],ax
     	or	ax,ax
	jz	nseq
	call	encode
	or	[tchgd],NEW_S
nseq:
	mov	al,[tchgd]
	test	al,al
	jnz	spcI
        mov     dx,[si].ip_length
        mov     ax,[di].ip_length
	cmp	ax,dx
        jz      ucg3
	xchg	al,ah
        push    cx
        shl     cx,2
	cmp	ax,cx
        pop     cx
        jnz     ucg3
	jmp	nospecial
spcI:
        cmp     al,SPECIAL_I    ; get out if the changes match a special case encoding
	jz	ucg3
	cmp	al,SPECIAL_D
	jz	ucg3
	cmp	al,NEW_S + NEW_A
	jnz	spcS
	mov	ax,[DeltaS]
	cmp	ax,[DeltaA]
	jnz	nospecial
reseq:
	mov	dx,[di].ip_length
	xchg	dl,dh
	sub	dx,cx
	sub	dx,cx
	sub	dx,cx
	sub	dx,cx
	cmp	dx,ax
	jnz	nospecial
	mov	[tchgd],SPECIAL_I
	mov	bp,offset cgroup:newhead
	jmp	nospecial
spcS:
	cmp	al,NEW_S
	jnz	nospecial
	mov	ax,[DeltaS]
	mov	dx,[di].ip_length
	xchg	dl,dh
	sub	dx,cx
	sub	dx,cx
	sub	dx,cx
	sub	dx,cx
	cmp	dx,ax
	jnz	nospecial
	mov	[tchgd],SPECIAL_D
	mov	bp,offset cgroup:newhead
nospecial:
	mov	ax,[si].ip_id
	xchg	al,ah
	mov	dx,[di].ip_id
	xchg	dl,dh
	sub	ax,dx
	cmp	ax,1
	jz	no_I
	or	[tchgd],NEW_I
	call	encodez
no_I:
	test	[si+bx].tcp_flags,TH_PSH
	jz	nopush
	or	[tchgd],TCP_PUSH_BIT
nopush:
	push	[si+bx].tcp_chksum
	push	di                	; move newheader to storage
	push	si
	push	cx
	add	cx,cx
	rep	movsw
	pop	cx
	pop	si
	pop	di
	add	cx,cx
	add	cx,cx			; get true header length
	lea	di,[di-cs_ip]
	mov	al,[di].cs_id		; al holds compression id
	mov	di,si
	mov	si,offset cgroup:newhead
	sub	bp,si                   ; bp holds comressed header len
	test	[xmitvjcompid],-1	; see if putting compressed CID
	jz	nocidcomp
	cmp	al,[sldat].last_xmit	; maybe, see if is same as last
	jnz	nocidcomp		; no, uncompressed CID
	sub	cx,3			; space for tcp chksum and compress modes
	sub	cx,bp			; space for rest of header
	add	di,cx			; DI points to space before the packet
	pop	dx
	push	di			; to hold new header
	mov	al,[tchgd]		; save modes
	stosb
	jmp	cidcont
nocidcomp:
	mov	[sldat].last_xmit,al	; save compression id
	mov	ah,al			; ah = CID
	sub	cx,4
	sub	cx,bp
	add	di,cx			; find place for new header
	pop	dx
	push	di
	mov	al,[tchgd]		; save modes
	or	al,NEW_C
	stosb
	xchg	al,ah			; and CID
	stosb
cidcont:
	mov	ax,dx		; TCP checksum into packet
	stosb
	xchg	al,ah
	stosb
	xchg	cx,dx		; dx holds header delta
	mov	cx,bp		; now move rest of new header into place
	rep	movsb
	pop	si   		; new packet pointer
	mov	di,[sldat].last_cs	; recalc len
	mov	di,[di].cs_next
	pop	cx
	sub	cx,dx
	mov	dx,IP_VJ_COMPRESSED_PROTOCOL
	ret
uncompressed:
        add     word ptr [vjstats].sls_o_uncompressed,1
        adc     word ptr [vjstats+2].sls_o_uncompressed,0
	mov	al,[di].cs_id
	lea	di,[di].cs_ip
	push	si
        push    ax
        call    getlen
        add     cx,cx
	rep	movsw
        pop     ax
	pop	si
	pop	cx
	mov	[si].ip_prot,al
        mov     [sldat].last_xmit,al
	mov	dx,IP_VJ_UNCOMPRESSED_PROTOCOL
endif
	ret
vjcompress ENDP
ifndef NOVJ
;
; decode something, returns it in NETWORK order in ax
;
bdecode PROC
	lodsb
	or	al,al
	jz	bd_double
	xchg	al,ah
	sub	al,al
	ret
bd_double:
	lodsw
bd_fin:
	ret
bdecode ENDP
;
; Decode something and add it to a (long) field
;
decodel PROC
	call	bdecode
decodel ENDP
add4	PROC
	add	ds:[bp+3],ah
	adc	ds:[bp+2],al
	adc	byte ptr ds:[bp+1],0
	adc	byte ptr ds:[bp],0
	ret
add4	 ENDP
;
; decode something and add it to a (short) field
;
decodes PROC
	call	bdecode
decodes ENDP
add2	PROC
	add	ds:[bp+1],ah
	adc	ds:[bp+0],al
	ret
add2	ENDP
checksum PROC
	push	si		; calculate IP checksum
        push    cx
	mov	cl,[si].ip_version
	and	cx,0fh
	add	cx,cx
	sub	dx,dx
csl:
	lodsw
	xchg	al,ah
	add	dx,ax
	adc	dx,0
	loop	csl
        pop     cx
	pop	si
	not	dx
	xchg	dl,dh
        ret
checksum ENDP
vjuncompress PROC
	cmp	ax,IP_VJ_UNCOMPRESSED_PROTOCOL	; uncompressed packet?
        jz      vjg
        jmp     vjccom
vjg:
	mov	al,[si].ip_prot
        cmp     al,[rcvvjcompstates]
	jb	vjucok
	sub	si,si
	jmp	vjerror
vjucok:
	sub	ah,ah	
	mov	[sldat].last_recv,al
	mul	[cssize]
	mov	di,offset cgroup:sldat.rstate
	add	di,ax
	and	[sldat].sflags,NOT SLF_TOSS
	mov	[si].ip_prot,IPP_TCP
	push	cx
	mov	bl,[si].ip_version	; find TCP header
	and	bx,0fh
	shl	bx,1
	shl	bx,1
	call	getlen
        add     cx,cx
        add     cx,cx
        mov     [di].cs_hlen,cx
	lea	di,[di].cs_ip
	push	si
	push	di
        shr     cx,1
	rep	movsw
	pop	di
	pop	si
	pop	cx
	mov	[di].ip_chksum,0
	mov	ax,IP_PROTOCOL
        add     word ptr [vjstats].sls_i_uncompressed,1
        adc     word ptr [vjstats+2].sls_i_uncompressed,0
        ; fall through and check checksum
vjuc1:  
        cmp     ax, IP_PROTOCOL
        jnz     ncks
        push    [si].ip_chksum
        mov     [si].ip_chksum,0
        push    ax
        call    checksum
        pop     ax
        pop     [si].ip_chksum
        cmp     dx,[si].ip_chksum
        jz      ncks
        add     word ptr [vjstats].sls_i_badcheck,1
        adc     word ptr [vjstats+2].sls_i_badcheck,0
ncks:
        ret
vjccom:
	cmp	ax,IP_VJ_COMPRESSED_PROTOCOL
	jnz	vjuc1
	push	si
	lodsb
	mov	[rchgd],al
	test	al,NEW_C
	jz	rnocid
	lodsb
        cmp     al,[xmitvjcompstates]
	jb	rcidok
	pop	si
	sub	si,si
	jmp	vjerror
rcidok:
	and	[sldat].sflags,NOT SLF_TOSS
	mov	[sldat].last_recv, al
	jmp	rcidj
rnocid:
	test	[sldat].sflags,SLF_TOSS
	jz	rcidj
        add     word ptr [vjstats].sls_i_tossed,1
        adc     word ptr [vjstats+2].sls_i_tossed,0
	pop	si
	sub	si,si
	mov	ax,IP_PROTOCOL
	ret
rcidj:
        add     word ptr [vjstats].sls_i_compressed,1
        adc     word ptr [vjstats+2].sls_i_compressed,0
	mov	al,[sldat].last_recv
	sub	ah,ah
	mul	[cssize]
	mov	di,offset cgroup:sldat.rstate
	add	di,ax
	mov	bl,[di].cs_ip.ip_version
	and	bx,15
	add	bx,bx
	add	bx,bx
	lodsw
	mov	[di+bx].cs_ip.tcp_chksum,ax
	and	[di+bx].cs_ip.tcp_flags, NOT TH_PSH
	test	[rchgd],TCP_PUSH_BIT
	jz	rnopush
	or	[di+bx].cs_ip.tcp_flags,TH_PSH
rnopush:
	mov	al,[rchgd]
	and	al,SPECIALS_MASK
	cmp	al,SPECIAL_I
	jnz	rspcD
	mov	ax,[di].cs_ip.ip_length
	xchg	al,ah
	sub	ax,[di].cs_hlen
	xchg	al,ah
	lea	bp,[di+bx].cs_ip.tcp_ack
	call	add4
	lea	bp,[di+bx].cs_ip.tcp_seq
	call	add4
	jmp	rddone
rspcd:
	cmp	al,SPECIAL_D
	jnz	rnospecial
	mov	ax,[di].cs_ip.ip_length
	xchg	al,ah
	sub	ax,[di].cs_hlen
	xchg	al,ah
	lea	bp,[di+bx].cs_ip.tcp_seq
	call	add4
     	jmp	rddone
rnospecial:
	and	[di+bx].cs_ip.tcp_flags, NOT TH_URG
	test	[rchgd],NEW_U
	jz	rnou
	or	[di+bx].cs_ip.tcp_flags,TH_URG
	call	bdecode
	mov	[di+bx].cs_ip.tcp_urp,ax
rnou:
	test	[rchgd],NEW_W
	jz	rnow
	lea	bp,[di+bx].cs_ip.tcp_wnd
	call	DecodeS
rnow:
	test	[rchgd],NEW_A
	jz	rnoa
	lea	bp,[di+bx].cs_ip.tcp_ack
	call	DecodeL
rnoa:
	test	[rchgd],NEW_S
	jz	rnos
	lea	bp,[di+bx].cs_ip.tcp_seq
	call	DecodeL
rnos:
rddone:
	lea	bp,[di].cs_ip.ip_id
	test	[rchgd],NEW_I
	jz	rincl
	call	DecodeS
	jmp	rid
rincl:
	mov	ax,100h		; constant 1 in network order
	call	add2
rid:
	pop	ax   		; ax = start of original buffer
	push	ax
	push	di
	push	ax
	sub	ax,si           ; negative of compression header size
	add	cx,ax		; everything but the compression header
	mov	ax,[di].cs_hlen	; amount to move up
	pop	di
	add	di,ax		; dest
	push	cx

	add	di,cx         	; point to end of block
	add	si,cx
	dec	di
	dec	di
	dec	si
	dec	si
	std
	inc	cx
	shr	cx,1
	rep	movsw
	cld
	pop	cx		; count
	pop	di		; compression struct
	pop	si		; sob
	add	cx,[di].cs_hlen
	push	cx
	xchg	cl,ch
	mov	[di].cs_ip.ip_length,cx

	push	di		; move header
	push	si
	xchg	si,di
	mov	cx,[si].cs_hlen
	lea	si,[si].cs_ip
        shr     cx,1
        rep     movsw
	pop	si
	pop	di

        call    checksum

        pop     cx
	mov	[si].ip_chksum,dx
	mov	ax,IP_PROTOCOL
	ret
vjuncompress ENDP
endif
;
; ip protocol handler
;
iphandle PROC
	push	ax
	test	ah,1
	jz	nocomp
	xchg	al,ah
nocomp:
	cmp	ax,IP_PROTOCOL		; straight IP, list it
	jz	listit
ifndef NOVJ
	cmp	ax,IP_VJ_UNCOMPRESSED_PROTOCOL ; compressed or uncompressed
	jz	decompress
	cmp	ax,IP_VJ_COMPRESSED_PROTOCOL
endif
	jnz	notus
ifndef NOVJ
decompress:
	call	vjuncompress	; then hit the uncompress routine
endif
listit:
	or	si,si		; if uncompressed to buffer = 0
	jz	tossed		; then it was a tossed packet and don't list
ifndef TESTVJ
	call	listedprothandler ; else list
endif
tossed:
	pop	ax
	clc
	ret
notus:
	pop	ax
	stc
	ret
iphandle ENDP
;
; called when the serial line comes up, clears the state struct
; then initializes things the way we need them
;
vjinit PROC
ifndef NOVJ
;
; clear the struct
;
	mov	cx,size slcompdat
	mov	di,offset cgroup:sldat
	sub	al,al
	rep	stosb
;
; disable CID compression... assumes we can't have a state number 255
;
	mov	al,255
	mov	[sldat].last_recv,al
	mov	[sldat].last_xmit,al
;
; link the tstates into a list
;
	mov	di,offset cgroup:sldat.tstate
	mov	ax,di
	mov	bx,di
	mov	cl,[xmitvjcompstates]
	sub	ch,ch
	dec	cx
	jcxz	tsnl
tsl:
	add	bx,size cstate
	mov	[di].cs_next,bx
	mov	[di].cs_id,cl
	mov	di,bx
	loop	tsl
tsnl:
	mov	[di].cs_next,ax
	mov	[di].cs_ip,0
	mov	[sldat].last_cs,di
endif
        ret
vjinit  endp
fillvjstats PROC
ifndef NOVJ
        sub     ax,ax
        mov     [vjstats.cstate_ptr_tstate],ax
        mov     [vjstats.cstate_ptr_rstate],ax
        mov     al,[xmitvjcompstates]
        dec     al
        mov     [vjstats.tslot_limit],al
        mov     al,[rcvvjcompstates]
        dec     al
        mov     [vjstats.rslot_limit],al
        mov     bx,[sldat.last_cs]
        mov     al,[bx].cs_id
        mov     [vjstats.xmit_oldest],al
        mov     al,[sldat].last_xmit
        mov     [vjstats.xmit_current],al
        mov     al,[sldat].last_recv
        mov     [vjstats.recv_current],al
        mov     [vjstats.slflags],0
endif
	ret
fillvjstats ENDP
TSR	ENDS
CONFIG	segment word public 'CODE'
ifdef TESTVJ
sbuf	db	1500 DUP (?)
IPHEAD macro len,s1,s2,s3,s4,d1,d2,d3,d4,ttl,offs,datalength, id
	db	len + 40h	; len & version
	db	5		; tos
	dw	(datalength SHR 8) + ((datalength AND 255) SHL 8); length
	dw	(id SHR 8) + ((id AND 255) SHL 8); id
	dw	offs		; offset
	db	ttl		; ttl
	db	IPP_TCP		; TCP protocol
	dw	0		; checksum
	db	s1,s2,s3,s4	; source
	db	d1,d2,d3,d4	; dest
	endm
TCPHEAD macro	source,dest,offs,flags,seq,ack,wnd,urgent
	dw	source,dest
	dd	seq
	dd	ack
	db	offs
	db	flags
	dw	wnd
	dw	3344h
	dw	urgent
	endm
test1	LABEL byte
	IPHEAD  5, 1,1,1,1, 2,2,2,2, 3, 0, 41, 1
	TCPHEAD 5, 6, 50h, TH_ACK,055443322h,088776655h,04422h, 0
	db	5
test1end LABEL byte
test2	LABEL byte
	IPHEAD  5, 1,1,1,2, 2,2,2,2, 3, 0, 41, 1
	TCPHEAD 5, 6, 50h, TH_ACK,055443322h,088776655h,04422h, 0
	db	5
test2end LABEL byte
test3	LABEL byte
	IPHEAD  5, 1,1,1,1, 2,2,2,2, 3, 0, 50, 2
	TCPHEAD 5, 6, 50h, <TH_ACK+TH_PSH>,057443322h,089876655h,03422h, 0
	db "1234567890"
test3end LABEL byte
test4	LABEL byte
	IPHEAD  5, 1,1,1,1, 2,2,2,2, 3, 0, 40, 3
	TCPHEAD 5, 6, 50h, <TH_ACK>,061443322h,089876655h,03422h, 0
test4end LABEL byte

t1      label byte
        db      45h,000h,000h,028h,000h,004h,000h,000h,0FEh,006h,065h,083h,0D8h,087h,05Eh,033h,0D9h,0C4h,046h,0C9h,004h,038h,000h,050h,0CDh,0FEh,0D2h,02Bh,061h,06Fh,051h,01Ch,050h,010h,008h,000h,0F9h,04Dh,000h,000h,000h,000h,000h,000h,000h,000h,0
t2      label byte
        db      45h,000h,000h,065h,000h,005h,000h,000h,0FEh,006h,065h,045h,0D8h,087h,05Eh,033h,0D9h,0C4h,046h,0C9h,004h,038h,000h,050h,0CDh,0FEh,0D2h,02Bh,061h,06Fh,051h,01Ch,050h,010h,008h,000h,01Fh,0F0h,000h,000h,047h,045h,054h,020h,02Fh,063h,070h,071h,075h,06Fh,074h,065h,06Eh,02Eh,070h,064h,066h,020h,048h,054h,054h,050h,02Fh,031h,02Eh,05Bh,030h,031h,05Dh,00Dh,00Ah,055h,073h,065h,072h,02Dh,041h,067h,065h,06Eh,074h,03Ah,020h,048h,054h,047h,045h,054h,02Dh,044h,04Fh,053h,02Fh,031h,02Eh,030h,035h,00Dh,00Ah,00Dh,00Ah,0
t3      label byte
        db      45h,000h,000h,065h,000h,006h,000h,000h,0FEh,006h,065h,044h,0D8h,087h,05Eh,033h,0D9h,0C4h,046h,0C9h,004h,038h,000h,050h,0CDh,0FEh,0D2h,02Bh,061h,06Fh,051h,01Ch,050h,018h,008h,000h,01Fh,0E8h,000h,000h,047h,045h,054h,020h,02Fh,063h,070h,071h,075h,06Fh,074h,065h,06Eh,02Eh,070h,064h,066h,020h,048h,054h,054h,050h,02Fh,031h,02Eh,05Bh,030h,031h,05Dh,00Dh,00Ah,055h,073h,065h,072h,02Dh,041h,067h,065h,06Eh,074h,03Ah,020h,048h,054h,047h,045h,054h,02Dh,044h,04Fh,053h,02Fh,031h,02Eh,030h,035h,00Dh,00Ah,00Dh,00Ah,0
t4      label byte
        db      45h,000h,000h,028h,000h,007h,000h,000h,0FEh,006h,065h,080h,0D8h,087h,05Eh,033h,0D9h,0C4h,046h,0C9h,004h,038h,000h,050h,0CDh,0FEh,0D2h,068h,061h,06Fh,056h,0D0h,050h,018h,002h,04Ch,0F9h,008h,000h,000h,000h,000h,000h,000h,000h,000h,0
t5      label byte

dumpbyte PROC
	push	ax
	call	dumphex
	mov	ah,2
	mov	dl,' '
	int	21h
	pop	ax
	ret
dumphex:
	push	ax
	shr	al,4
	call	dumpnib
	pop	ax
dumpnib:
	and	al,0fh
	add	al,'0'
	cmp	al,'9'
	jle	dno
	add	al,7
dno:
	mov	dl,al
	mov	ah,2
	int	21h
	ret
dumpbyte ENDP
dumpit PROC
	push	dx
	push	cx
	push	si
	push	dx
	mov	ah,2
	mov	dl,13
	int	21h
	mov	ah,2
	mov	dl,10
	int	21h
	pop	ax
	xchg	al,ah
	call	dumpbyte
	xchg	al,ah
	call	dumpbyte
dlp:
	lodsb
	call	dumpbyte
	loop	dlp

		
	pop	si
	pop	cx
	pop	dx
	ret
dumpit ENDP
runit proc
	mov	di,offset cgroup:sbuf
	push	di
	push	cx
	rep	movsb
	pop	cx
	pop	si
	call	dumpit
	call	vjcompress
	call	dumpit
	mov	ax,dx
	call	vjuncompress
	mov	dx,ax
	call	dumpit
	ret
runit	endp
start:
	push	cs
	pop	ds
	push	cs
	pop	es
	cld
	call	vjinit
	mov	[xmitvjcomp],1
;	mov	[xmitvjcompid],1
ifdef NORM
	mov	si,offset cgroup:test1
	mov	cx,offset cgroup:test1end - offset cgroup:test1
	mov	dx,0021h
	call	runit

	mov	si,offset cgroup:test2
	mov	cx,offset cgroup:test2end - offset cgroup:test2
	mov	dx,0021h
	call	runit

	mov	si,offset cgroup:test3
	mov	cx,offset cgroup:test3end - offset cgroup:test3
	mov	dx,0021h
	call	runit

	mov	si,offset cgroup:test4
	mov	cx,offset cgroup:test4end - offset cgroup:test4
	mov	dx,0021h
	call	runit
else
        mov     si,offset cgroup:t1
        mov     cx,offset cgroup:t2 - cgroup:t1
	mov	dx,0021h
	call	runit
        mov     si,offset cgroup:t2
        mov     cx,offset cgroup:t3 - cgroup:t2
	mov	dx,0021h
	call	runit
        mov     si,offset cgroup:t3
        mov     cx,offset cgroup:t4 - cgroup:t3
	mov	dx,0021h
	call	runit
        mov     si,offset cgroup:t4
        mov     cx,offset cgroup:t5 - cgroup:t4
	mov	dx,0021h
	call	runit

endif


	mov	ah,4ch
	int	21h
endif	
CONFIG	ENDS
        end