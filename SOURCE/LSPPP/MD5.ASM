;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
;TESTmd5 EQU 1
ifdef TESTmd5
	.model small
	.stack
endif
;
; shift value constants
;
S11 =7
S12 =12
S13 =17
S14 =22

S21 =5
S22 =9
S23 =14
S24 =20


S31 =4
S32 =11
S33 =16
S34 =23

S41 =6
S42 =10
S43 =15
S44 =21

;
; step descriptor structure
;
stepd struc                            
px	db	0
inidx   db      0
sval    db      0
num     dd      0
stepd   ends

CGROUP group TSR,CONFIG
	assume cs:cgroup,ds:cgroup
	public md5_init, md5_buf, md5_update, md5_final
TSR segment word public 'CODE'
md5_temp dd	2 DUP (0)	; temp for the i values
md5_i   dd      2 DUP (0)	; i values (number of bytes)
md5_buf dd      4 DUP (0)	; hash buffer
md5_in  db      64 DUP (0)	; input buffer
md5_init_str dd     067452301h,0efcdab89h,098badcfeh,010325476h
;
; step temps
;
a       dd      0		; these must be in order		
b       dd      0
c       dd      0
d       dd      0
;
; step descriptor list
;
Descs Label byte
  stepd < 000011011b,   0, S11, 3614090360 > ;   1  
  stepd < 011000110b,  1, S12, 3905402710 > ;   2  
  stepd < 010110001b,  2, S13,  606105819 > ;   3  
  stepd < 001101100b,  3, S14, 3250441966 > ;   4  
  stepd < 000011011b,   4, S11, 4118548399 > ;   5  
  stepd < 011000110b,  5, S12, 1200080426 > ;   6  
  stepd < 010110001b,  6, S13, 2821735955 > ;   7  
  stepd < 001101100b,  7, S14, 4249261313 > ;   8  
  stepd < 000011011b,   8, S11, 1770035416 > ;   9  
  stepd < 011000110b,  9, S12, 2336552879 > ;  10  
  stepd < 010110001b, 10, S13, 4294925233 > ;  11  
  stepd < 001101100b, 11, S14, 2304563134 > ;  12  
  stepd < 000011011b,  12, S11, 1804603682 > ;  13  
  stepd < 011000110b, 13, S12, 4254626195 > ;  14  
  stepd < 010110001b, 14, S13, 2792965006 > ;  15  
  stepd < 001101100b, 15, S14, 1236535329 >  ;  16  
  stepd < 000011011b,   1, S21, 4129170786 > ;  17  
  stepd < 011000110b,  6, S22, 3225465664 > ;  18  
  stepd < 010110001b, 11, S23,  643717713 > ;  19  
  stepd < 001101100b,  0, S24, 3921069994 > ;  20  
  stepd < 000011011b,   5, S21, 3593408605 > ;  21  
  stepd < 011000110b, 10, S22,   38016083 > ;  22  
  stepd < 010110001b, 15, S23, 3634488961 > ;  23  
  stepd < 001101100b,  4, S24, 3889429448 > ;  24  
  stepd < 000011011b,   9, S21,  568446438 > ;  25  
  stepd < 011000110b, 14, S22, 3275163606 > ;  26  
  stepd < 010110001b,  3, S23, 4107603335 > ;  27  
  stepd < 001101100b,  8, S24, 1163531501 > ;  28  
  stepd < 000011011b,  13, S21, 2850285829 > ;  29  
  stepd < 011000110b,  2, S22, 4243563512 > ;  30  
  stepd < 010110001b,  7, S23, 1735328473 > ;  31  
  stepd < 001101100b, 12, S24, 2368359562 >  ;  32  
  stepd < 000011011b,   5, S31, 4294588738 > ;  33  
  stepd < 011000110b,  8, S32, 2272392833 > ;  34  
  stepd < 010110001b, 11, S33, 1839030562 > ;  35  
  stepd < 001101100b, 14, S34, 4259657740 > ;  36  
  stepd < 000011011b,   1, S31, 2763975236 > ;  37  
  stepd < 011000110b,  4, S32, 1272893353 > ;  38  
  stepd < 010110001b,  7, S33, 4139469664 > ;  39  
  stepd < 001101100b, 10, S34, 3200236656 > ;  40  
  stepd < 000011011b,  13, S31,  681279174 > ;  41  
  stepd < 011000110b,  0, S32, 3936430074 > ;  42  
  stepd < 010110001b,  3, S33, 3572445317 > ;  43  
  stepd < 001101100b,  6, S34,   76029189 > ;  44  
  stepd < 000011011b,   9, S31, 3654602809 > ;  45  
  stepd < 011000110b, 12, S32, 3873151461 > ;  46  
  stepd < 010110001b, 15, S33,  530742520 > ;  47  
  stepd < 001101100b,  2, S34, 3299628645 >  ;  48  
  stepd < 000011011b,   0, S41, 4096336452 > ;  49  
  stepd < 011000110b,  7, S42, 1126891415 > ;  50  
  stepd < 010110001b, 14, S43, 2878612391 > ;  51  
  stepd < 001101100b,  5, S44, 4237533241 > ;  52  
  stepd < 000011011b,  12, S41, 1700485571 > ;  53  
  stepd < 011000110b,  3, S42, 2399980690 > ;  54  
  stepd < 010110001b, 10, S43, 4293915773 > ;  55  
  stepd < 001101100b,  1, S44, 2240044497 > ;  56  
  stepd < 000011011b,   8, S41, 1873313359 > ;  57  
  stepd < 011000110b, 15, S42, 4264355552 > ;  58  
  stepd < 010110001b,  6, S43, 2734768916 > ;  59  
  stepd < 001101100b, 13, S44, 1309151649 > ;  60  
  stepd < 000011011b,   4, S41, 4149444226 > ;  61  
  stepd < 011000110b, 11, S42, 3174756917 > ;  62  
  stepd < 010110001b,  2, S43,  718787259 > ;  63  
  stepd < 001101100b,  9, S44, 3951481745 >  ;  64  
;
; extra bytes for padding
;
padbyte db 80h,0

;si = x
; di = y
; bp = z
;
; (X & Y) | (~X & Z)
;
funcf PROC
	xchg	si,di		; si = y, di = x, bp = z
fcomplete:
        mov     bx,[si]		; y and x (or x and z)
        mov     cx,[si+2]
        and     bx,[di]
        and     cx,[di+2]
        xchg    si,bp		; si = z, bp = y (or  si = y, bp = x)
        mov     ax,[di]		; z and not x (or y and not z)
        mov     dx,[di+2]
	not	ax
	not	dx
        and     ax,[si]
        and     dx,[si+2]
        or      ax,bx		; or them together
        or      dx,cx
        ret
funcf ENDP
;
; (X & Z) | (Y & ~Z )
;
funcg PROC
        xchg    bp,di		; si = x, di = z, bp = y
        jmp     fcomplete
funcg ENDP
;
; X ^ Y ^ Z
;
funch PROC
        mov     ax,[si]		; x ^ y
        mov     dx,[si+2]
        xor     ax,[di]
        xor     dx,[di+2]
hcomplete:
        xchg    di,bp		; di = z 
        xor     ax,[di]		; x ^ y ^ z or (y ^ (~z | x))
        xor     dx,[di+2]
        ret
funch ENDP
;
; Y ^ (X | ~ Z)
;
funci PROC
        xchg    bp,di
        mov     ax,[di]		; ~z | x
        mov     dx,[di+2]
	not	ax
	not	dx
        or     ax,[si]
        or     dx,[si+2]
        jmp     hcomplete
funci ENDP

;
; *p1 = f(*p2,*p3,*p4 + num + ((unsigned long *)md5_in)[inidx] + *p1
; *p1 <<= *sval
; *p1 += *p2
;
load1 PROC
	mov	cx,bx
	and	cl,3
	shl	cl,1
	shl	cl,1
	add	cx,offset cgroup:a
	shr	bl,1
	shr	bl,1
	ret
load1 ENDP
	
	
funcxx PROC
	push	cx
        push    si
	mov	bl,[si].px
	sub	bh,bh
	call	load1
	mov	bp,cx		; bp = p4
	call	load1
	mov	di,cx		; di = p3
	call	load1
	mov	si,cx		; si = p2
	push	si
	call	load1
	push	cx
        call    ax
	pop	di		; holds p1
	pop	bx		; holds p2
        pop     si		; database again
	push	bx		; p2 back to stack
        add     ax,word ptr [si].num
        adc     dx,word ptr [si+2].num
        mov     bl,[si].inidx
        sub     bh,bh
        shl     bl,1
        shl     bl,1
        lea     bx,[bx + md5_in]
        add     ax,word ptr [bx]
        adc     dx,word ptr [bx+2]
        add     ax,word ptr [di]
        adc     dx,word ptr [di+2]
	mov	cl,[si].sval
	sub	ch,ch
        jcxz    ffnoshift
ffl:
        shl     ax,1
        rcl     dx,1
        jnc     ffno0
        or      al,1
ffno0:
        loop    ffl
ffnoshift:
	pop	bp		; p2
        add     ax,word ptr ds:[bp]
        adc     dx,word ptr ds:[bp+2]
        mov     word ptr [di],ax
        mov     word ptr [di+2],dx
	pop	cx
        ret
funcxx ENDP

;
; initialize hasher
;
md5_init proc
	cld
        mov     di,offset cgroup:md5_i
        mov     cx,4
        sub     ax,ax
        rep     stosw
        mov     si,offset cgroup:md5_init_str
        mov     cx,8
        rep     movsw
        ret
md5_init endp
;
; update md5 data
;
md5_update PROC
        cld
        mov     di,word ptr [md5_i]
        and     di,03fh
        lea     di,[md5_in+di]
        add     word ptr [md5_i],cx     ; length has to be shifted at end
        adc     word ptr [md5_i+2],0
        adc     word ptr [md5_i+4],0
        adc     word ptr [md5_i+6],0
        ;assumes cx nonzero
mulp:
        movsb
        cmp     di,offset cgroup:md5_in + 64
        jnz     muc
        call    transform               ; push all
        mov     di,offset cgroup:md5_in
muc:
        loop    mulp
        ret
md5_update ENDP
;
; pad, stick on the length, and do a final hash
;
md5_final PROC
        cld
        mov     di,offset cgroup:md5_temp
        push    di
        push    di
        mov     si,offset cgroup:md5_i
        mov     cx,4
        rep     movsw

        pop     di
        mov     cx,3
mfl:
        shl     word ptr [di],1
        rcl     word ptr [di+2],1
        rcl     word ptr [di+4],1
        rcl     word ptr [di+6],1
        loop    mfl

        mov     cx,word ptr [md5_i]
        and     cx,3fh
        cmp     cx,56
        jb      lt56
        neg     cx
        add     cx,120
        jmp     ltj
lt56:
        neg     cx
        add     cx,56
ltj:
	push	cx
        mov     si,offset cgroup:padbyte
        mov     cx,1
        call    md5_update
	pop	cx
	dec	cx
	jcxz	lta
lt1:
        push    cx
        mov     si,offset cgroup:padbyte+1
        mov     cx,1
        call    md5_update
        pop     cx
        loop    lt1
lta:

        pop     si
        mov     di,offset cgroup:md5_in+56
        mov     cx,4
        rep     movsw
        ; fall through
md5_final ENDP
;
; run the transform on a buffer full of data
;
transform PROC
        push    cx
        push    si
        push    di
        mov     si,offset cgroup:md5_buf
        mov     di,offset cgroup:a
        mov     cx,8
        rep     movsw

        mov     cx,16
        mov     si,offset cgroup:descs
        push    cx
lp1:
        mov     ax,offset cgroup:funcf
        call    funcxx
        add     si,size stepd
        loop    lp1
        pop     cx
        push    cx
lp2:
        mov     ax,offset cgroup:funcg
        call    funcxx
        add     si,size stepd
        loop    lp2
        pop     cx
        push    cx
lp3:
        mov     ax,offset cgroup:funch
        call    funcxx
        add     si,size stepd
        loop    lp3
        pop     cx
lp4:
        mov     ax,offset cgroup:funci
        call    funcxx
        add     si,size stepd
        loop    lp4

        mov     si,offset cgroup:a
        mov     di,offset cgroup:md5_buf
        mov     cx,4
lp5:
        lodsw
        add     [di],ax
        lodsw
        adc     [di+2],ax
        add     di,4
        loop    lp5
ifdef TESTmd5
	call	dumpbuf
endif
        pop     di
        pop     si
        pop     cx
        ret
transform ENDP
TSR ends
CONFIG	segment word public 'CODE'
ifdef TESTmd5
ibuf	db	1024 DUP (0)
ipfile	db	"md5.dat",0
noopen	db	"Can't open md5.dat",10,13,'$'
hexbuf  db	"   $"
dumphex PROC
	mov	di,offset cgroup:hexbuf
	push	ax
	shr	al,4
	call	dumpnib
	pop	ax
dumpnib:
	and	al,0fh
	add	al,30h
	cmp	al,3ah
	jb	duok
	add	al,7
duok:
	stosb
	ret
dumphex ENDP
start:
	mov	ax,cs
	mov	ds,ax
	mov	es,ax
	call	md5_init
	mov	ax,3d00h		; open file
	sub	cx,cx
	mov	dx,offset cgroup:ipfile
	int	21h
	jnc	okopen		; err if can't open, just exit
	mov	dx,offset cgroup:noopen
	mov	ah,9
	int	21h
x:
	mov	ax,4c01h
	int	21h
okopen:
	mov	bx,ax
	mov	cx,1024
	mov	dx,offset cgroup:ibuf
	mov	ah,3fh
	int	21h
	jc	x
  	push	ax
	mov	ax,3e00h
	int	21h
	pop	cx
	mov	si,offset cgroup:ibuf
	call	md5_update
	call	md5_final
	call	dumpbuf
	mov	ax,4c00h
	int	21h

dumpbuf:
	.286
	pusha
	mov	si,offset cgroup:md5_buf
	mov	cx,16
vv:
	lodsb
	call	dumphex
	push	si
	push	cx
	mov	dx,offset cgroup:hexbuf
	mov	ah,9
	int	21h
	pop	cx
	pop	si
	loop	vv
	mov	ah,2
	mov	dl,13
	int	21h	
	mov	ah,2
	mov	dl,10
	int	21h	
	popa
	ret
endif
CONFIG	ends
	end