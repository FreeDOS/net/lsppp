;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
include ppp.asi
CGROUP group TSR,CONFIG
	assume cs:cgroup,ds:cgroup

	public loginit, logsend, logrecv, tolog, logrundown, writeconfig
        public netmask, formbyte2, logtime
	extrn xmitbuf : byte, peermru : word
	extrn ipaddr : dword, hisipaddr: dword, dns1: dword, dns2: dword
	extrn gettime : PROC

TSR segment word public 'CODE'
netmask dw	0,0
tolog	db	0
logtime dw    0
filename db	"lsppp.log",0
obuf	db	3,"   "
crlf	db	2,13,10
send	db	6,"send: "
rcv	db	6," rcv: "
timebuf db      8," 00000  "
xcts    db      3,"CTS"
xncts   db      5,"NOCTS"
datseg dw       ?
datloc  dw      0

;
; start logging
;
loginit	proc
	test	[tolog],1		; get out if no log file
	jz	li_none
        mov     bx,1000h
        mov     ah,48h
        int     21h
        jc      nomem
	mov	[datseg],ax
        push    ax
	call	gettime
        mov     [logtime],ax
        pop     ax
        push    es
        mov     es,ax
        sub     di,di
        mov     cx,8000h
        mov     ax,'  '
        rep     stosw
        pop     es
        or      [tolog],2
li_none:
	ret
nomem:
        mov     [tolog],0
        ret
loginit	endp
;
; end logging
;
logrundown PROC
        test    [tolog],2               ; get out if no log file
	jz	lrd_ni
	mov	[tolog],0		; and logging
        mov     ax,3c00h                ; open file
	sub	cx,cx
	mov	dx,offset cgroup:filename
	int	21h
        jc      lrd_ni2
        push    ax
        push    ds
        push    [datloc]
        mov     bx,ax
        push    bx
        mov     cx,[datloc]
        mov     ds,[datseg]
        mov     dx,cx
        neg     cx
        jcxz    ar1
        mov     ax,4000h
        int     21h
ar1:
        pop     bx
        pop     cx
        sub     dx,dx
        jcxz    ar2
        mov     ax,4000h
        int     21h
ar2:
        pop     ds
	mov	ax,3e00h		; close file
	pop	bx
	int	21h
lrd_ni2:
        push    es
        mov     ah,49h
        mov     es,[datseg]
        int     21h
        pop     es
lrd_ni:
        mov     [tolog],0
	ret
logrundown ENDP
;
; write something to log buffer
;
write PROC
        push    si
        push    di
        push    es
        mov     si,dx                   ; get length byte
        mov     cl,[si]
        inc     si                      ; point to data
	sub	ch,ch
        mov     es,[datseg]
        mov     di,[datloc]
        mov     ax,di
        add     ax,cx
        cmp     ax,65500
        jnc      nocopy
        cld
        rep     movsb
        mov     [datloc],di
nocopy:
        pop     es
        pop     di
        pop     si
	ret
write ENDP
ifdef EXLOG
        public readtx,writetx,logcts, logncts
xtx	db	5,13,10,"TX:"
xrx	db	5,13,10,"RX:"
	.286
writetx proc
        test    [tolog],2
	jz	wtx
	pusha
	push 	ax
	mov	dx,offset cgroup:xtx
	call	write
	pop	ax
	call	writebyte
	popa
wtx:
	ret
writetx endp
readtx 	proc
        test [tolog],2
	jz	rtx
	pusha
	push 	ax
	mov	dx,offset cgroup:xrx
	call	write
	pop	ax
	call	writebyte
	popa
rtx:
	ret
readtx	endp
logcts  PROC
        test [tolog],2
        jz      lctx
	pusha
        mov     dx,offset cgroup:xcts
	call	write
	popa
lctx:
	ret
logcts  ENDP
logncts PROC
        test [tolog],2
        jz      lnctx
	pusha
        mov     dx,offset cgroup:xncts
	call	write
	popa
lnctx:
	ret
logncts ENDP
	.8086
endif
;
; write a CR/LF pair out
;
writecrlf PROC
	mov	dx,offset cgroup:crlf
	jmp	write
writecrlf ENDP
;
; format a byte for writing , standard to-hex converter
;
formbyte PROC
	mov  di,offset cgroup:obuf+1	; get output buf
formbyte2:
	push ax				; save char
	shr  al,4			; get high nib
	call outnib			; out it
        pop ax				; get low nib
outnib:
	and al,0fh			; isolate nibble
	cmp al,10			; >= 10?
	jc  no7				; no, it is a number
 	add al,7			; else adjust for letter
no7:
	add al,30h			; make ascii
	stosb				; save it
	ret
formbyte	endp
;
; format and write a byte to disk
;
writebyte PROC
	call	formbyte		; format it
	mov	dx,offset cgroup:obuf	
	call	write
	ret
writebyte ENDP
;
; send a buffer out
;
ripout  PROC
	lodsb				; get char
	push	si
	push	cx
	call 	writebyte		; write it
	pop	cx
	pop	si
	loop	ripout			; loop till done
	ret
ripout  ENDP
ifdef XXXXX
ripoutw  PROC
	lodsb				; get char
        cmp     al,7dh
        jnz     rp2
        dec     cx
        lodsb
        xor     al,20h
rp2:
	push	si
	push	cx
	call 	writebyte		; write it
	pop	cx
	pop	si
        loop    ripoutw                 ; loop till done
	ret
ripoutw  ENDP
endif
;
; put a timestamp in the buffer
;
timestamp PROC
	call	gettime
        sub     ax,[logtime]
        mov     di,offset cgroup:timebuf + 6
	mov	bx,10
        mov     cx,5
tsl:
	sub	dx,dx
	div	bx
	add	dl,'0'
	mov	[di],dl
	dec	di
	loop	tsl
	mov	dx,offset cgroup:timebuf
	jmp	write
timestamp ENDP
;
; write the send buffer
;
logsend	proc
        test    [tolog],2         ; get out if no log file
	jz	lsni
	push	ax			; push all regs
	push	bx
	push	cx
	push	dx
	push	si
	push	di

	push	ax			; write banner
	mov	dx,offset cgroup:send
	call	write
	call	timestamp
	pop	ax

        mov     bx,ax                   ; get count

	mov	si,offset cgroup:xmitbuf ; go dump it
        lodsb
        dec     si
        cmp     al,0ffh
        jnz     lsg
        lodsw
        sub     bx,2
lsg:
        mov     cx,bx
        call    ripout
	call	writecrlf

	pop	di			; restore regs
	pop	si
	pop	dx
	pop	cx
	pop	bx
	pop	ax
lsni:
	ret
logsend	endp
ifndef XXXXX
public logvv
vv      db      6,"VV:   "
logvv proc
        test    [tolog],2         ; get out if no log file
        jz      lsni1
	push	ax			; push all regs
	push	bx
	push	cx
	push	dx
	push	si
	push	di

	push	ax			; write banner
        mov     dx,offset cgroup:vv
	call	write
	call	timestamp
	pop	ax

        mov     bx,ax                   ; get count

        mov     cx,bx
        call    ripout
	call	writecrlf

	pop	di			; restore regs
	pop	si
	pop	dx
	pop	cx
	pop	bx
	pop	ax
lsni1:
	ret
logvv endp
endif
;
; write the receive buffer;
;
logrecv proc
        test    [tolog],2         ; get out if no log file
	jz	lrni
	push	ax
	push	bx
	push	cx
	push	dx
	push	si
	push	di

	push	cx			; write banner
	push	si
	push	ax
	mov	dx,offset cgroup:rcv
	call	write
	call	timestamp
	pop	ax

	push	ax			; put out protocol
	xchg	al,ah
	call	writebyte
	pop	ax
	or	al,al
	jz	nosecond
	call	writebyte
nosecond:
	pop	si
	pop	cx
	call	ripout			; rip out the rest of the buf
	call	writecrlf

	pop	di
	pop	si
	pop	dx
	pop	cx
	pop	bx
	pop	ax
lrni:
	ret
logrecv endp
TSR	ENDS
CONFIG segment word public 'CODE'
ipfile	db	"ip-up.bat",0
sset	db	4,"set "
smyip	db	5,"myip="
sremip	db	6,"remip="
snetmask db	8,"netmask="
speermru db	8,"peermru="
sdns1	db	5,"dns1="
sdns2	db	5,"dns2="
sipaddr   db	15,"   .   .   .   "
sipcaddr db	16 dup (0)
speern	db	4,"    "


ihandle dw 0
;
; generic write
;
iwrite proc
	mov	bx,dx			; point to data
	mov	cl,[bx]			; get len
	inc	dx			; point past len
	sub	ch,ch			;
	mov	bx,[ihandle]		; get handle
	mov	ax,4000h		; write
	int	21h
	ret
iwrite endp
icrlf PROC
	mov	dx,offset cgroup:crlf
	jmp	iwrite
icrlf ENDP
wset	PROC
	mov	dx,offset cgroup:sset
	jmp	iwrite
wset	ENDP
;
; write a number out
;
writenum proc
	mov	di,dx
	add	di,cx
	dec	di
	push	dx
	std
wnl:
	push	cx
	sub	dx,dx
	mov	cx,10
	div	cx
	add	dl,'0'
	xchg	al,dl
	stosb
	xchg	al,dl
	pop	cx
	or	ax,ax
	jz	dnx
	loop	wnl
	cld
	pop	dx
	ret
dnl:
	mov	al,' '
	stosb
dnx:
	loop	dnl
	cld
	pop	dx
	ret
writenum endp
compressip proc
	mov	cx,15
	mov	si,offset cgroup:sipaddr+1
	mov	di,offset cgroup:sipcaddr+1
cil:
	lodsb
	cmp	al,' '
	jz	cilk
	stosb
cilk:
	loop	cil
	sub	di,offset cgroup:sipcaddr+1
	mov	cx,di
	mov	[sipcaddr],cl
	ret
compressip endp
;
; write an ip address
;
writeip proc
	push	bx
	push	ax
	mov	dx,offset cgroup:sipaddr+1
	mov	cx,3
	sub	ah,ah
	call	writenum
	pop	ax
	xchg	al,ah
	mov	dx,offset cgroup:sipaddr+5
	mov	cx,3
	sub	ah,ah
	call	writenum
	pop	ax
	push	ax
	mov	dx,offset cgroup:sipaddr+9
	mov	cx,3
	sub	ah,ah
	call	writenum
	pop	ax
	xchg	al,ah
	mov	dx,offset cgroup:sipaddr+13
	mov	cx,3
	sub	ah,ah
	call	writenum
	call	compressip
	mov	dx,offset cgroup:sipcaddr
	call	iwrite
	ret
writeip	endp
;
; get the net mask
;
getnetmask proc
        mov     ax,0ffh
        sub     dx,dx
        test    byte ptr [ipaddr],80h
        jz      gotnetmask
        mov     ax,0ffffh
        test    byte ptr [ipaddr],40h
        jz      gotnetmask
        mov     dx,0ffh
        test    byte ptr [ipaddr],20h
        jz      gotnetmask
        mov     dx,0ffffh
gotnetmask:
gnlp:
        mov     bx,word ptr [ipaddr]
        mov     cx,word ptr [ipaddr+2]
        mov     si,word ptr [hisipaddr]
        mov     di,word ptr [hisipaddr+2]
        and     bx,ax
        and     cx,dx
        and     si,ax
        and     di,dx
        cmp     bx,si
        jnz     mult
        cmp     cx,di
        jz      gnmx
mult:
        shl     dh,1
        rcl     dl,1
        rcl     ah,1
        rcl     al,1
        jmp     gnlp        
        
gnmx:
        mov     [netmask],ax
        mov     [netmask +2],dx
        mov     bx,dx
        ret
getnetmask endp
;                  
; write the configuration file
;
noconfig:
	ret
writeconfig proc
	mov	ax,3c00h		; open file
	sub	cx,cx
	mov	dx,offset cgroup:ipfile
	int	21h
	jc	noconfig		; err if can't open, just exit
	mov	[ihandle],ax		; save handle
	call	wset
	mov	dx,offset cgroup:smyip
	call	iwrite
	mov	ax,word ptr [ipaddr]
	mov	bx,word ptr [ipaddr + 2]
	call	writeip
	call	icrlf
	call	wset
	mov	dx,offset cgroup:sremip
	call	iwrite
	mov	ax,word ptr [hisipaddr]
	mov	bx,word ptr [hisipaddr + 2]
	call	writeip
	call	icrlf
	call	wset
	mov	dx,offset cgroup:snetmask
	call	iwrite
	call	getnetmask
	call	writeip
	call	icrlf
	call	wset
	mov	dx,offset cgroup:speermru
	call	iwrite
	mov	ax,[peermru]
	mov	cx,4
	mov	dx,offset cgroup:speern+1
	call	writenum
	dec	dx
	call	iwrite
	call	icrlf
	mov	ax,word ptr [dns1]
	or	ax,word ptr [dns1 + 2]
	jz	nodns
	call	wset
	mov	dx,offset cgroup:sdns1
	call	iwrite
	mov	ax,word ptr [dns1]
	mov	bx,word ptr [dns1 + 2]
	call	writeip
	call	icrlf
	mov	ax,word ptr [dns2]
	or	ax,word ptr [dns2 + 2]
	jz	nodns
	call	wset
	mov	dx,offset cgroup:sdns2
	call	iwrite
	mov	ax,word ptr [dns2]
	mov	bx,word ptr [dns2 + 2]
	call	writeip
	call	icrlf
nodns:
	mov	ax,3e00h		; close file
	mov	bx,[ihandle]
	int	21h
	ret
	
writeconfig endp
CONFIG	ENDS
	end