;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
include ppp.asi

P_INQUOTE = 1

SWITCH_ERR 	= 1
BAD_TIME_ERR	= 2
BAD_COUNT_ERR	= 3
BAD_BAUD_ERR	= 4
OUR_VEC_ERR	= 5
BAD_IRQ		= 6
BAD_COM_ERR	= 7
NEED_COLON	= 8
NEED_COMMA	= 9
BAD_IP_ERR	= 10
BAD_FIFO_ERR 	= 11
BAD_MRU_ERR     = 12

; a:$ - asyncmap
; b:$ - commport
; V:# - commvect
; n:# - commnum
; v:$ - vector
; B:# - baudrate
; S:# - suggest an IP address
; L:#,# - lcp params
; P:$ - password
; U:$ - user
; A:#,# - auth params
; D:#,# - dial params
; I:#,# - ipcp params
; N:#[,#] - name servers
; E - load as ethernet
; d[r]:$ - dial (phone number)
; l - log file
; u - terminate TSR

cgroup group tsr,config
assume	cs:cgroup
assume	es:nothing
assume  ds:nothing
ENVIRON = 2ch
COMLINE = 81h            
tsr segment word public 'CODE'
	public	clflags, parse_comline
	extrn	baseport : word, printtext : proc
	extrn setport : proc
     	extrn commnum : byte,commport : word ,commvect : byte,baudrate : word,vector : byte
	extrn	act : byte, passwd : byte, authtime: word, authcount : word
	extrn	dial_timeout : word,dial_number : byte,dial_retries : byte
	extrn printtext : proc
	extrn LCP_struc : statemachine, IPCP_struc : statemachine
	extrn dial_flags : byte
	extrn tolog : byte, features : byte
	extrn dns1: dword, dns2: dword, dialstr : byte
	extrn xltabxmit : dword, defcontmap : dword, dial_finish:word
        extrn GetNameservAddress : PROC, fifo_setting : byte
        extrn ipaddr : byte, peermru : word, receivemru : word ;
        extrn fifo_tx : byte

clflags dw      E_OPTION        ; command line option flags
TSR	ends
config segment word public 'CODE'
badmruerr       db      "MRU must be 1500 octets or less$"
badfifoerr	db	"Bad FIFO setting$"
badiperr	db	"Bad IP value$"
needcomma	db	"Need a comma$"
needcolon	db	"Need a colon$"
badcomerr	db	"Bad comm port number$"
badcommvect	db	"Bad comm port IRQ$"
ourvecerr	db	"Bad vector specified$"
badbauderr	db	"Baud rate must be at least 100$"
badcounterr	db	"Count must be between 1 and 255$"
badtimeerr	db	"Time must not exceed 1600 seconds$"
switcherr	db	"Unknown command$"
on		db	" on /$"
switch		db	" switch",13,10,'$'
envname		db	"LSPPP="
ENVNAMELEN	EQU	$-envname
errtab		dw	cgroup:switcherr,cgroup:badtimeerr,cgroup:badcounterr,cgroup:badbauderr
		dw	cgroup:ourvecerr,cgroup:badcommvect,cgroup:badcomerr, cgroup:needcolon,cgroup:needcomma
                dw      cgroup:badiperr,cgroup:badfifoerr,cgroup:badmruerr
helpmsg		db	"usage: lsppp [options]"
                db      9,9,"PPP, Copyright (C) LADSoft, 1999-2003",13,10,13,10
		db	"/h or ?",9,"This text",13,10

		db	"/a:%",9,"Set Async map",9,9,9
                db      "/B:#",9,"Baud rate",9,9,9

                db      "/b:%",9,"Base i/o port",9,9,9
                db      "/F:#[,#] Fifo setting",13,10

                db      "/d[r]:*",9,"Dial [redial] phone number",9
		db	"/D:#,#,# Dial timeout, count, and delay",13,10

                db      "/i",9,"Comm IRQ (0-15)",9,9,9
                db      "/I:#,#",9,"IPCP timeout & count",13,10

                db      "/l",9,"Local mode",9,9,9
		db	"/L:#,#",9,"LCP timeout & count",13,10

                db      "/m:#,#",9,"Set transmit and receive MRUs",9
		db	"/M:*",9,"Modem init string",13,10

		db	"/n:#",9,"Comm port num",9,9,9
		db	"/N:@,@",9,"DNS address",13,10

                db      "/p",9,"Load as native ppp driver",9
                db      "/P:*",9,"Set passwordd",13,10

		db	"/u",9,"Unload TSR",9,9,9
                db      "/S:@",9,"Suggest IP addr",13,10

                db      "/x",9,"XON/XOFF enabled",9,9
                db      "/U:*",9,"Set user name",13,10      

                db      "/A:#,#",9,"PAP timeout & count",9,9
		db	"/V:%",9,"Packet driver vector",13,10

                db      13,10
		db	9,"* = a string (possibly quoted)",13,10
		db	9,"# = a number",9,"@ = ip address",9,"% = hex number",13,10
		db	9,"retry counts in range 1 - 255",13,10
		db	9,"timeouts in seconds, max 1600; 0 = no timeout",13,10
                db      13,10
                db      9,"LSPPP will read parameters from LSPPP.CFG, then",13,10
                db      9,"from the environment, then from the command line"
		db	"$"
pflags	db	0		; parser flags
filestring      label   
envstr          db      256 DUP (0)
filename        db      260 DUP (0)
switchval	db	0
inbyte          db      0
;
; parameter vectors
;
params	label byte
	db	'h'
	dw	df_help
	db	'?'
	dw	df_help
	db	'a'
	dw	df_async_map
	db	'b'
	dw	df_commport
	db	'i'
	dw	df_commvect
	db	'l'
	dw	df_local
	db	'n'
	dw	df_commnum
	db	'V'
	dw	df_vector
	db	'x'
	dw	df_xonxoff
	db	'B'
	dw	df_baudrate
	db	'L'
	dw	df_lcpparms
	db	'M'
	dw	df_dialstring
	db	'P'
	dw	df_passwd
	db	'U'
	dw	df_user
	db	'A'
	dw	df_authparms
	db	'D'
	dw	df_dialparms
	db	'd'
	dw	df_dial
	db	'I'
	dw	df_ipcpparms
	db	'u'
	dw	df_unload
        db      'p'
        dw      df_ppp
	db	'N'
	dw	df_nameserver
	db	'F'
	dw	df_fifosettings
	db	'z'
	dw	df_logfile
        db      'S'
        dw      df_suggestedip
        db      'm'
        dw      df_mrus
	db	0         	; mark end of tab
;
; now come input parsing routines for command lines
;
; first skip a spaces
;
skipspace PROC
	lodsb
	cmp	al,' '
	jz	skipspace
	dec	si
	; fall through
skipspace ENDP
;
; get a char and return C set if is end of text
;
nextchr PROC
	lodsb
	or	al,al
	jz	eosp
	cmp	al,13
	jz	eosp
	clc
	ret
eosp:
	dec	si
	stc
	ret
nextchr ENDP
colon	PROC
	call	nextchr
	cmp	al,':'
	mov	dl,NEED_COLON
	jnz	perr
	ret
colon	endp
comma	PROC
	call	nextchr
	cmp	al,','
	mov	dl,NEED_COMMA
	jnz	perr
	ret
comma	endp

;
; input a number, 0 -65535
;
ten	dw	10
parsenum PROC
	sub	cx,cx	; result = 0;
pel:
	call	nextchr	; get a char
	jc	pex	; exit if done
	sub	al,'0'
	jc	peerr	; exit if less than 0
	cmp	al,10
	jae	peerr	; or greater than 10
	sub	ah,ah
	xchg	cx,ax
	mul	[ten]	; else multiply result by 10
	add	cx,ax	; and add in new digit
	or	dx,dx
	jz	pel
	mov	cx,0ffffh ; max out- used to force a divisor of one if they
			; use a high baud rate...
	jmp	pel	; and loop
peerr:
	dec	si	; point back at first non-number
	clc
pex:
	mov	ax,cx	; return result in AX
	ret
parsenum ENDP
;
; return to caller's caller with an error
;
perr	PROC
	pop	ax
	stc
	ret
perr	endp
;
; input a number, hex, 0 -65535
;
parsehex PROC
	sub	cx,cx	; result = 0;
	sub	dx,dx
phl:
	call	nextchr	; get a char
	jc	phx	; exit if done	
	cmp	al,40h
	jc	phcont
	and	al,0dfh
	cmp	al,'X'	; let them use the 'X' character...
	jz	phl
	sub	al,7
phcont:
	sub	al,'0'
	jc	pherr	; exit if less than 0
	cmp	al,16
	jae	pherr	; or greater than 16
phgo:
	shl	cx,1	; multiply by 16
	rcl	dx,1
	shl	cx,1
	rcl	dx,1
	shl	cx,1
	rcl	dx,1
	shl	cx,1
	rcl	dx,1
	or	cl,al	; and add in new digit
	jmp	phl	; and loop
pherr:
	dec	si	; point back at first non-number
	clc
phx:
	mov	ax,cx	; return result in dx:AX
	ret
parsehex ENDP
;
; need a time in the range 0 - 1600
; 0 = no timeout
;
needtime PROC
	call	parsenum
	cmp	ax,1600
	mov	dl,BAD_TIME_ERR
	ja	perr
	mov	cx,182
	mul	cx
	mov	cx,10
	div	cx
	clc
	ret
needtime ENDP
;
; need a count in the range 1 - 255
;
needcount PROC
	call	parsenum
	mov	dl,BAD_COUNT_ERR
	or	al,al
	jz	perr
	cmp	ax,256
	jnc	perr
	clc
	ret
needcount ENDP
;
; input a counted string
;
parsestring PROC
	mov	cx,STRINGLEN-1	; max len
	push	di		; save pointer to count
	inc	di		; point to first data
psl:
	call	nextchr		; get a char
	jc	psx		; out if none
	test	[pflags],P_INQUOTE ; if in quote accept spaces
	jnz	literal
	cmp	al,' '		; else space ends this
	jz	psx
	cmp	al,9
	jz	psx
literal:
	cmp	al,'"'		; toggle quote flag if quote
	jnz	store
	xor	[pflags],P_INQUOTE
	jmp	psl
store:
	stosb      		; else store char
	loop	psl		; loop
psx:
	pop	di		; point at count field
	mov	ax,STRINGLEN-1	; calc len
	sub	ax,cx
	stosb			; store it
pse:
	ret
parsestring ENDP
;
; input an IP
;
parseip PROC
	call	GetNameservAddress
parseip2:
	mov	cx,4		; 4 numbers
pil:
	push	cx		; save pointers
	push	di
	call	parsenum	; get a num
	pop	di
	pop	cx
	
	or	ah,ah	
	jnz	badiperrx	; err if too big
	stosb			; save it
	cmp	cl,1		; get out if last
	jz	pix		;
	call	nextchr		; else look for .
	jc	badiperrx	; get out if empty
	cmp	al,'.'		; else is dot
	jnz	badiperrx	; err if not
	loop	pil		; continue (always branches)
pix:
	clc
	ret
badiperrx:
	mov	dl,BAD_IP_ERR
	jmp	perr
parseip ENDP
;
; command line params
;
df_local PROC
	or clflags,L_OPTION OR NOMODEM_OPTION
	ret
df_local ENDP
df_xonxoff PROC
	or clflags,X_OPTION OR NOMODEM_OPTION
        or word ptr cgroup:[xltabxmit+2],000Ah
        or word ptr cgroup:[defcontmap+2],0A00h
	ret
df_xonxoff ENDP
df_help	PROC
ifdef EXE
	push	ds
	push	cs
	pop	ds
endif
	mov	dx,offset cgroup:helpmsg
	call	printtext
ifdef EXE
	pop	ds
endif
	jmp	perr
df_help	ENDP
df_async_map PROC
	call	colon
	call	parsehex
        mov      word ptr cgroup:[xltabxmit],ax
        mov      word ptr cgroup:[xltabxmit+2],dx
	xchg	al,ah
	xchg	dl,dh
        mov      word ptr cgroup:[defcontmap+4],ax
        mov      word ptr cgroup:[defcontmap+2],dx
        clc
	ret
df_async_map ENDP
df_mrus PROC
        call    colon
        call    parsenum
        cmp     ax,1500
        ja      badmru
        mov     word ptr cgroup:[peermru],ax
        cmp     byte ptr [si],','
        jnz     df_mrux
        call    nextchr
        call    parsenum
        cmp     ax,1500
        ja      badmru
        mov     word ptr cgroup:[receivemru],ax
df_mrux:
        clc
        ret
badmru:
        mov     dl,BAD_MRU_ERR
        stc
        ret
df_mrus ENDP
df_commport	PROC
	call	colon
	call	parsehex
	clc
	mov	[commport],ax
	ret
df_commport	ENDP
df_commvect	PROC
	call	colon
	call	parsenum
	add	al,8
	cmp	al,16
	jc	okcommvect
	test	[features],TWOPICS
	jz	cve
	add	al,70h-10h
	cmp	al,78h
	jc	okcommvect
cve:
	mov	dl,BAD_IRQ
	stc
	ret
okcommvect:
	clc	
	mov	[commvect],al
	ret
df_commvect	ENDP
df_commnum	PROC
	call	colon
	call	parsenum
	dec	al
	js	dfcn_err
	cmp	al,4
	jae	dfcn_err
	mov	[commnum],al
	call	setport
	or	[clflags],CN_OPTION
	ret
dfcn_err:
	mov	dl,BAD_COM_ERR
	stc
	ret
df_commnum	ENDP
df_fifosettings PROC
	call	colon
	call	parsenum
	sub	ah,ah
	cmp	al,1
	jz	df_fsok
	mov	ah,040h
	cmp	al,4
	jz	df_fsok
	mov	ah,80h
	cmp	al,8
	jz	df_fsok
	mov	ah,0c0h
	cmp	al,14
	jz	df_fsok
badfifo:
	mov	dl,BAD_FIFO_ERR
	stc
	ret
df_fsok:
        or      ah,1
	mov	[fifo_setting],ah
        cmp     byte ptr [si],','
        jnz     df_fsfinal
        call    nextchr
        call    parsenum
        cmp     al,16
        ja      badfifo
        or      al,al
        jz      badfifo
        mov     [fifo_tx],al
df_fsfinal:
        clc
	ret
df_fifosettings ENDP
df_vector	PROC
	call	colon
	call	parsehex
	clc
	mov	[vector],al
	ret
df_vector	ENDP
df_baudrate	PROC
	call	colon
	call	parsenum
	cmp	ax,100
	jc	dfbe
	or	[clflags],B_OPTION
	mov	[baudrate],ax
	clc
	ret
dfbe:
	stc
	mov	dl,BAD_BAUD_ERR
	ret
df_baudrate	ENDP
df_lcpparms	PROC
	call	colon
	call	needtime
	mov	[LCP_struc].sm_deftime,ax
	call	comma
	call	needcount
	mov	[LCP_struc].sm_defcount,al
	ret
df_lcpparms	ENDP
df_ipcpparms	PROC
	call	colon
	call	needtime
	mov	[IPCP_struc].sm_deftime,ax
	call	comma
	call	needcount
	mov	[IPCP_struc].sm_defcount,al
	ret
df_ipcpparms	ENDP
df_passwd	PROC
	call	colon
	mov	di,offset cgroup:passwd
	jmp	parsestring
df_passwd	ENDP
df_dialstring	PROC
        cmp     byte ptr [si],':'
        jnz     df_dsx
	call	colon
	mov	di,offset cgroup:dialstr
	push	di
	call	parsestring
	pop	di
	jc	df_dsx
	inc	byte ptr cs:[di]		; append a CR
	mov	bl,cs:[di]
	sub	bh,bh
	mov	byte ptr cs:[di+bx],13
df_dsx:
	ret
df_dialstring	ENDP
df_user	PROC
	call	colon
	mov	di,offset cgroup:act
	jmp	parsestring
df_user	ENDP
df_authparms	PROC
	call	colon
	call	needtime
	mov	[authtime],ax
	call	comma
	call	needcount
	mov	[authcount],ax
	ret
df_authparms	ENDP
df_dialparms	PROC
	call	colon
	call	needtime
	mov	[dial_timeout],ax
	call	comma
	call	needcount
	mov	[dial_retries],al
	cmp	byte ptr [si],','
	clc
	jnz	dfdp_done
	call	comma
	call	needtime
	mov	[dial_finish],ax
dfdp_done:
	ret
df_dialparms	ENDP
df_dial PROC
	cmp	byte ptr [si],'r'
	jz	dial1
	cmp	byte ptr [si],'R'
	jnz	dial2
dial1:
	inc	si
	or	[clflags],DN_OPTION
	jmp	df_dial
dial2:
	cmp	byte ptr [si],'m'
	jz	dial3
	cmp	byte ptr [si],'M'
	jnz	dial4
dial3:
	inc	si
	or	[clflags],DM_OPTION
	jmp	df_dial
dial4:
	cmp	byte ptr [si],'f'
	jz	dial5
	cmp	byte ptr [si],'F'
	jnz	dial6
dial5:
	inc	si
	or	[clflags],DF_OPTION
	jmp	df_dial
dial6:
	cmp	byte ptr [si],'l'
	jz	dial7
	cmp	byte ptr [si],'L'
	jnz	dial8
dial7:
	inc	si
	or	[clflags],DL_OPTION
	jmp	df_dial
dial8:
	cmp	byte ptr [si],'e'
	jz	dial9
	cmp	byte ptr [si],'E'
	jnz	dial10
dial9:
	inc	si
	or	[clflags],DE_OPTION
	jmp	df_dial
dial10:
	or	[dial_flags],SHOULD_DIAL
	call	colon
	mov	di,offset cgroup:dial_number
	jmp	parsestring
df_dial ENDP
df_unload PROC
	or	[clflags],U_OPTION
	ret
df_unload ENDP
df_ppp PROC
        and     [clflags],NOT E_OPTION
	ret
df_ppp ENDP
df_logfile PROC
	or	[tolog],1
	ret
df_logfile ENDP
df_nameserver PROC
	call	colon		; get the colon
	call	parseip		; parse it
	call	nextchr		; get next char
	jc	dfnsx		; out if no more
	cmp	al,','		; see if semi
	jz	dfnsg		; out if not
      	cmp	al,';'
	jz	dfnsg
	cmp	al,':'
	jz	dfnsg
	cmp	al,'+'
	jnz	dfnsx2
dfnsg:
	call	parseip		; has to be a call
	inc	si		; make up for next
dfnsx2:
	dec	si
dfnsx:
	clc
	ret
df_nameserver	ENDP
df_suggestedip PROC
        call    colon
        mov     di,offset cgroup:ipaddr
        call    parseip2
        clc
        ret
df_suggestedip ENDP
chkname	PROC
	push	si
	mov	di,offset cgroup:envname
	mov	cx,ENVNAMELEN
	repe	cmpsb
	jz	cne
	pop	si
scalp:
	lodsb
	or	al,al
	jnz	scalp
	test	byte ptr [si],-1
	jnz	chkname
	stc
	ret
cne:
	add	sp,2
	ret
chkname	ENDP
;
; main command line parser
; parses both environ and command line
;
parse_comline PROC
	push	cs
	pop	es
        call    parse_configfile
ifdef EXE
	mov 	ax,cs
	sub 	ax,10h
	mov 	ds,ax
endif
	mov	ds,DS:[ENVIRON]	; load environ
	sub	si,si		;
pcllp:
	call	chkname		; see if current is it
	jc	noenv		; yep, got
gotenv:
	mov	di,offset cgroup:envstr ; load copy params
	mov	cx,255
gel:
	lodsb			; copy to our dseg
	stosb
	or	al,al
	loopnz	gel
	sub	al,al		; store a terminator in case too big
	stosb

	mov	si,offset cgroup:envstr; parse it
	mov	ax,cs
	call	parse_line
noenv:
      	mov	si,COMLINE	; now parse command line
				; we do this second to override defaults
	mov 	ax,cs		; point back at the PSP
ifdef EXE
	sub	ax,10h
endif

parse_comline	ENDP
parse_line PROC
	mov	ds,ax
pcl2:
	call	skipspace	; next nonspace character
	jnc	pck		; continue if not done
pcd:
	clc
ifdef EXE
	push	cs
	pop	ds
endif
	ret			; quit if done
pck:
	cmp	al,'-'		; check for a switch char
	jz	parseswitch
	cmp	al,'/'
	mov	dl,SWITCH_ERR	; expected switch
	jnz	badcomline	; get out if neither
parseswitch:
	call	nextchr  	; expect a specifier
	mov	[switchval],al
	mov	dl,SWITCH_ERR	; expected switch
	jc	badcomline	; error if not
	mov	bx,offset cgroup:params; point at params table
psl1:
	test	byte ptr cs:[bx],-1; see if done
	mov	dl,SWITCH_ERR
	jz	badcomline	; yes- unkown param
	cmp	al,cs:[bx]		; see if got
	jz	branch		; yes go do it
	add	bx,3		; point to next table ent
	jmp	psl1		; loop
branch:
	call	word ptr cs:[bx+1]	; call parse routine
	jnc	pcl2		; loop if no error
parse_line ENDP
badcomline PROC
ifdef EXE
	push	cs
	pop	ds
endif
	assume ds:cgroup
	mov	bl,dl
	sub	bh,bh
	shl	bx,1
	mov	dx,[bx+errtab-2]
	call	printtext
	mov	dx,offset cgroup:on
	call	printtext
	mov	ah,2
	mov	dl,[switchval]
	int	21h
	mov	dx,offset cgroup:switch
	call	printtext
	stc
	ret
badcomline ENDP
parse_configfile PROC
        push    ds
ifdef EXE
	mov 	ax,cs
	sub 	ax,10h
	mov 	ds,ax
endif
	mov	ds,DS:[ENVIRON]	; load environ
	sub	si,si		;
ilp:
        lodsb
        or      al,al
        jnz     ilp
        lodsb
        or      al,al
        jnz     ilp
        lodsw
        mov     di,offset cgroup:filename
ilp2:
        lodsb
        stosb
        or      al,al
        jnz     ilp2
        mov     byte ptr es:[di-4],'C'
        mov     byte ptr es:[di-3],'F'
        mov     byte ptr es:[di-2],'G'
        pop     ds
        mov     ax,3d00h                ; open file
	mov	dx,offset cgroup:filename
	int	21h
        jc      pcf_x
        mov     bx,ax
cfglp:
        call    getfileline
        jc      pcf_x_2
        push    bx
        mov     si,offset cgroup:filestring
        mov     ax,cs
        call    parse_line
        pop     bx
        jmp     cfglp
pcf_x_2:
	mov	ax,3e00h		; close file
	int	21h
pcf_x:        
        ret
        
parse_configfile ENDP
getfileline PROC
        mov     cx,256
        mov     di,offset cgroup:filestring
gfllp:
        push    cx
        mov     ah,3fh
        mov     cx,1
        mov     dx,offset cgroup:inbyte
        int     21h
        pop     cx
        jc      gflx
        cmp     ax,0
        jz      gfeof
        mov     al,[inbyte]
        cmp     al,13
        jz      gfllp
        cmp     al,10
        jz      gfend
gfstore:
        stosb
        loop    gfllp
gfeof:
        cmp     cx,256
        jnz     gfend
        stc
gflx:
        ret
gfend:
        mov     al,13
        stosb
        clc
        ret
        
getfileline ENDP
config ENDS
	end