;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
include ppp.asi

PAP_REQ = 1
PAP_ACK = 2
PAP_NAK = 3

PAP_RETRIES = 4
CGROUP group TSR,CONFIG
	assume cs:cgroup,ds:cgroup
	public PAP_INIT,pap_handler, authenticated, authtime, act, passwd
	public pap_retry, authcount 
	extrn confbuf : byte, startxmit : PROC
	extrn LCP_struc : statemachine, IPCP_struc : statemachine
	extrn ipcp_init : PROC
ifndef NOCHAP
	extrn	MD5_init:proc, MD5_update:proc, MD5_final:proc, md5_buf:byte
endif
TSR segment word public 'CODE'
authenticated	db	AU_AUTH	; assume authenticated, will be reset
				; if host negotiates for it using LCP
ifndef NOCHAP
chapid	db	0
endif
act	db	STRINGLEN DUP (?)
passwd	db	STRINGLEN DUP (?)
authtime dw	3 * 18
authcount dw	10
authtimeleft dw	0
retry	db	1 ; PAP_RETRIES
pap_id	db	1

;
; move a counted string
;
copystr PROC
	lodsb
	dec	si
	inc	al
	sub	ah,ah
	mov	cx,ax
	rep	movsb
	ret
copystr	ENDP
;
; send a login request
;
pap_sendreq PROC
	mov	di,offset cgroup:confbuf	; confbuf
	mov	al,PAP_REQ		; login command, ID=255
	mov	ah,[pap_id]
	inc	[pap_id]
	stosw
	mov	dx,4h			; save a dummy length
	stosw
	mov	si,offset cgroup:act		; copy account info
	call	copystr
	add	dx,ax
	mov	si,offset cgroup:passwd	; copy passwd info
	call	copystr
	add	dx,ax			; add into len field
	mov	si,offset cgroup:confbuf	; start a transmit
	mov	cx,dx
	xchg	dl,dh
	mov	word ptr [si+2],dx
	mov	ax,PAP_PROTOCOL
	jmp	StartXmit
pap_sendreq ENDP
ifndef NOCHAP
chap_sendresp PROC
	mov	di,offset cgroup:confbuf	; store in confbuf
	push	di				; buffer to stack
	mov	al,2				; this is a response
	stosb
	mov	al,[chapid]			; with their id
	stosb
	mov	al,[act]			; get act name length
	sub	ah,ah
	add	ax,21				; add in size of header and md5 bytes
	push	ax				; save to stack
	xchg	al,ah				; store in stream
	stosw
	mov	ax,16				; length of mdi data seg
	stosb
	mov	si,offset cgroup:md5_buf	; md5 data
	mov	cx,ax
	rep	movsb				; to buffer
	mov	si,offset cgroup:act		; account
	lodsb					; len
	mov	cl,al				; to buffer
	rep 	movsb		
	pop	cx              		; total len
	pop	si				; buffer
	mov	ax,CHAP_PROTOCOL		; CHAP
	jmp	StartXmit			; send it
chap_sendresp ENDP
endif
;
; papinit - send a login request if we aren't logged in
; this may be negotiated through LCP
;
PAP_INIT	PROC
	test	[authenticated],AU_INITTED
	jnz	papx
	mov	ax,1			; otherwise pap, get us going
	mov	[authtimeleft],ax
	cmp	[lcp_struc].sm_state,ST_OPENED
	jnz	papx
	test	[authenticated],AU_AUTH
	jnz	ipcpg
ifndef NOCHAP
	test	[authenticated],AU_CHAP	; chap, they ask first...
	jnz	papx
endif
	push	si
	push	bx
	call	pap_sendreq
	pop	bx
	pop	si
	or	[authenticated],AU_INITTED
papx:
	ret
;
; if there was no need for authorization (fat chance!)
; then we go on and do IPCP runup
ipcpg:
	or	[authenticated],AU_INITTED
	ret
PAP_INIT	ENDP

pap_retry	PROC
	sub	[authtimeleft],ax
	jg	noretry
	dec	authcount
	jl	paperr
ifndef NOCHAP
	test	[authenticated],AU_CHAP
	jz	prispap
	test	[authenticated],AU_CHAPREQ
	jz	prjn
	call	chap_sendresp
	jmp	prjn

prispap:
endif
	extrn	logrundown : PROC
;	call	logrundown
	call	pap_sendreq
prjn:
	mov	ax,[authtime]
	mov	[authtimeleft],ax
noretry:
	clc
	ret
paperr:
	stc
	ret
pap_retry	ENDP

pap_handler PROC
ifndef NOCHAP
	cmp	ax,CHAP_PROTOCOL
	jz	chap_handler
endif
	cmp	ax,PAP_PROTOCOL
	jz	ispap
	cmp	ax,PAP_PROTOCOL2
	jnz	notpap
ispap:
	cmp	byte ptr [si],PAP_ACK	; if they liked us,
	jz   	yespap			; we are logged in
;
; at this point we have a NAK, take action
	dec	[retry]
	jnz	resend
	or	[authenticated],AU_BADPASS
	clc
	ret
resend:
	jmp	pap_sendreq
;
; acknowledged, go on to next phase...
;
yespap:
        mov     bx,offset cgroup:IPCP_struc
        cmp     [bx].sm_state,ST_INITIAL
        jnz     noipcpinit
        call    ipcp_init
noipcpinit:
	and	[authenticated],AU_CHAP
	or	[authenticated],AU_AUTH OR AU_INITTED
	mov	[retry],PAP_RETRIES
	clc
	ret
notpap:
	stc
	ret
pap_handler ENDP
;
; If we get here we have an incoming CHAP message
;
ifndef NOCHAP
chap_handler PROC
	mov	ax,[si]
	cmp	al,1		; is it a challenge
	jz	answerchallenge
	cmp	al,3      	; is it success
	jz	yespap
	cmp	al,4		; is it failure?
	jnz	ignorechap	; none of the above, just ignore packet
	and	[authenticated],NOT AU_CHAPREQ
	or	[authenticated],AU_BADPASS	; failure, tell the connect routine
ignorechap:
	clc
	ret

answerchallenge:
	and	[authenticated],NOT AU_INITTED
	or	[authenticated],AU_CHAPREQ
	mov	al,[si+1]		; get id
	mov	[chapid],al
	push	si			; pointer to packet
	call	MD5_init		; init for hash

	pop	si      	; hash the response id
	inc	si
	mov	cx,1
	push	si
	call	MD5_update

	mov	si,offset cgroup:passwd	; hash the secret
	lodsb
	mov	cl,al
	sub	ch,ch
	call	MD5_update

	pop	si        	; hash the challenge value
	add	si,3
	lodsb
	mov	cl,al
	sub	ch,ch
	call	MD5_UPDATE
	
	call	MD5_final	; finish out the hash
	jmp	chap_sendresp
	
chap_handler ENDP
endif

TSR	ENDS
CONFIG	segment word public 'CODE'
CONFIG	ENDS
	end