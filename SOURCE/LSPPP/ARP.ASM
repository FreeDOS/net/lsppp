;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
include ppp.asi
include internet.asi

OP_REPLY = 2

	public arp_ouraddress, arp_handler, arp_copyaddress
	extrn confbuf : byte, listedprothandler : PROC

cgroup group tsr,config
assume	cs:cgroup
assume	ds:cgroup

TSR segment word public 'CODE'
address	db	4ch,4ch,4ch,0c4h,0c4h,0c4h
;
; these next must remain in this order...
thaddress db	0c4h,0c4h,0c4h,04ch,04ch,04ch
targip	db	4 DUP (?)
theirEA db	6 DUP (?)
theirip db	4 DUP (?)
;
; routine returns our ethernet address, which is just a made up number
; since this is PPP
;
arp_ouraddress PROC
	mov	si,offset cgroup:address
	mov	cx,3
	rep	movsw
	ret
arp_ouraddress ENDP
;
; create a mac address field
;
arp_copyaddress PROC
	push	si
	mov	di,si
	mov	si,offset cgroup:address	; destination
	mov	cx,3
	rep	movsw
	mov	si,offset cgroup:thaddress	; source
	mov	cx,3
	rep	movsw
	pop	si
	ret
arp_copyaddress ENDP
;
; routine reads the ARP records from their request
; into our local data area.  Not needed, just I'm lazy this day
;
get_info	PROC
	add	si,4		; point past the address space stuff, assume
				; they are doing ethernet like me :)
	lodsw			; ah = protocol lenght, al = hardware length
	add	si,2		; assume a request, they shouldn't be sending
				; responses without a request to them...
				; and we aren't going to request any
	mov	di,offset cgroup:theirEA ; get their address
	mov	cl,al
	sub	ch,ch	
	rep	movsb
	mov	di,offset cgroup:theirip ; and their IP
	mov	cl,ah
	rep	movsb
	mov	cl,al		; skip their assumption of the request address
	add	si,cx
	mov	di,offset cgroup:targip ; and the IP they are requesting an address for
	mov	cl,ah
	rep	movsb
	ret
get_info endp
;
; ARP packet handler
; parses their packet and sends a strange response
;
; numbers in this routine are little-endian
;
arp_handler PROC
	push	si
	push	di
	push	cx
	push	es
	push	ds
	pop	es
	lea	di,[si+14]		; exit if asking for own address
	lea	si,[si+24]
	mov	cx,4
	repe	cmpsb
	pop	es
	pop	cx
	pop	di
	pop	si
	jz	noresp
	call	get_info		; read their packet
	push	cs			; back to our dseg
	pop	ds
	mov	di,offset cgroup:confbuf	; output buffer
	push	di
	mov	ax,HNT SHL 8		; layer type (network addressing)
	stosw	
	mov	ax,ELT_IP		; Protocol IP (network addressing)
	stosw
	mov	al,6h			; length of hardware addresses
	stosb
	mov	al,4			; length of Protocol info for IP
	stosb
	mov	ax,OP_REPLY SHL 8	; this is a reply packet
	stosw
	mov	si,offset cgroup:thaddress	; we are pretting to be their target
	mov	cx,10 			; copy our stuff over...
	rep	movsw
	pop	si
	sub	di,si			; and length
	mov	cx,di
	mov	ax,ELT_ARP		; and protocol
	call	listedprothandler	; and send it to them.
noresp:
	push	cs
	pop	ds
	stc
	ret

arp_handler ENDP
TSR	ENDS
CONFIG	segment word public 'CODE'
CONFIG	ENDS
	end
