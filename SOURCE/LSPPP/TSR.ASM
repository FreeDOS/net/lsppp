;    LSPPP - DOS PPP Packet Driver
;    Copyright (C) 1997-2003  David Lindauer
;
;    This program is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
;
;    (for GNU General public license see file COPYING)
;
;    you may contact the author at:  mailto::camille@bluegrass.net
; 
;    or by snail mail at:
;
;    David Lindauer
;    850 Washburn Ave.  Apt #99
;    Louisville, KY 40222
;
include ppp.asi  
;PORTFOLIO = 1	; this is not debugged!!!!

;LOGATUNLOAD = 1

ECHO_PERIOD = 10 * 60 * 18

ifdef EXE
	.model small
        .stack  1024
endif

cgroup group tsr,resflag,config
assume	cs:cgroup
assume	ds:cgroup
	public	starttime, elapsedtime
	public SendFirstByte, IsXmitDone, unhookvects, termisr, drivername
	public	baseport, gettime
     	public commnum,commport,commvect,baudrate,vector, printtext, setport
	public freemem, features, receiveddispatch, nocts
        public ctrlbrk, fifo_setting, fifo_tx
ifdef PORTFOLIO
	extrn UninstallPortfolioInterrupt : PROC
	extrn InstallPortfolioInterrupt : PROC
	extrn CheckForPortfolio : PROC
endif
	extrn RemoveRecvBuffer : PROC, writeconfig : proc, pkt_init : PROC
	extrn logrecv : PROC , FreeBuffer : PROC, lcp_handler : PROC
	extrn loginit : PROC, logrundown : PROC
	extrn DecodePacketByte : PROC, FetchPacketByte : PROC, PPPDown : PROC
	extrn LCPISR : PROC, packetint : PROC, pdmsg : byte, p_class
	extrn	dial_xmit_isr : PROC,dial_rcv_isr : PROC, dial_flags : byte
	extrn	dial_timeout : word,dial_number : byte,dial_retries : word
	extrn	dialer : PROC, dial_init : PROC, formbyte2 : PROC
	extrn connect : PROC, clflags : word, parse_comline : PROC
	extrn numservers : byte, baseservers : byte, hangup : PROC
	extrn LCP_struc : statemachine, IPCP_struc : statemachine
        extrn authenticated : byte, SendEchoReq : PROC
	extrn sm_timed : PROC, pap_retry : PROC
	extrn ipcp_init : PROC, lcp_init : PROC
tsr segment word public 'CODE'
;
; program starts here
;
ifndef EXE
	org	100h
endif

start:
	jmp	go	
banner:
        db      "LADSoft PPP Packet Driver Ver 1.0 Copyright (c) LADSoft",13,10,"$"
drivername:
	db	"LADSoftPPP$",0
	even
commport	dw	0	; the comm port port value
commnum	db	0		; comm port number
commvect	db	4	; comm port vect
baudrate dw	19200		; comm port baudrate
picmask db	0		; the AND mask for pic enable
picnum	db	0		; the PIC we have to acknowledge
oldcommvect dd	?		; old vector of COMM port
oldvector	dd	?	; old vector of packet driver
old8		dd	?	; old int 8 vector
features db	0		; BIOS features byte
cputype db 	0		; CPU type (0/1/2)
vector	db	0		; which vector the driver is using
timebase dw	0		; last time we looked at the clock during delays
nocts	db	0		; Set if CTS not in effect
ctrlbrk db	0		; set if ctrl-break pressed
ifndef PORTFOLIO
baseport db	60h
else
baseport db	62h
endif
fifo_setting db	0		; receive fifo trigger level
fifo_tx         db      1       ; number of fifo tx bytes
fifo_on         db      0       ; fifo is on
receiveddispatch	db	0 ; true while dispatching received messsages
speoi	db	0		; the eoi to use
commflag db	0
echoreqtimer    dw      ECHO_PERIOD
;
; space for stack
;
	dw	64 DUP (0)
local_stack	label byte
;
; get a time change
;
gettime PROC
ifdef PORTFOLIO
	push	dx
	mov	dx,8040h	; read from portfolio time port
	in	al,dx		;
	sub	ah,ah		; clear upper byte
	pop	dx
else
	push	es		; get BIOS seg
	mov	ax,40h
	mov	es,ax
	mov	ax,es:[6ch]	; number of ticks
	pop	es
endif
	ret
gettime ENDP
;
; return amount of time that has elapsed in 18.2Hz ticks
;
elapsedtime PROC
	call	gettime		; get time
	mov	dx,[timebase]	; and old time
	sub	dx,ax		; get diff
	jz	notelapsed	; no diff, exit
	mov	[timebase],ax	; else update time
	jge	noneg		; see if overflow
	neg	dx		; yes, negate
noneg:
ifdef PORTFOLIO
	mov	ax,dx		; for portfolio, clock updates every
	shl	ax,1		; half second or nine ticks
	shl	ax,1
	shl	ax,1
	add	ax,dx
else
	mov	ax,dx		; otherwise we have clock update rate
endif
	ret
notelapsed:
	sub	ax,ax		; no time has elapsed
	ret

elapsedtime ENDP
starttime PROC
	call	gettime
	mov	[timebase],ax
	ret
starttime ENDP
;
; communications interrupt
;
circv	PROC
	push	dx
	in	al,dx
ifdef EXLOG
	extrn 	readtx:proc
	call	readtx
endif
	test	[dial_flags],DIALING
	jnz	circvdial
	call	DecodePacketByte
	jmp	circvx
circvdial:
	call	dial_rcv_isr
circvx:
	pop	dx  
	add	dx,5            	; keep going until all chars received
	in	al,dx
	sub	dx,5
	test	al,1
	jnz	circv
	ret
circv	endp
IsXmitDone PROC
	push	ax
	push	dx
	mov	dx,[commport]
	add	dl,5
	in	al,dx
	and	al,40h
	pop	dx	
	pop	ax
	ret
IsXmitDone	ENDP
SendFirstByte proc
	mov	dx,[commport]	; get the comm port
SendFirstByte endp
cixmit	PROC
        mov     cx,1
        test    [fifo_on],1
        jz      cifflp
	add	dl,5
	in	al,dx
        sub     dl,5
        test    al,20h
        jz      cifflp
        mov     cl,[fifo_tx]
cifflp:
	test	[nocts],0ffh	; get out if no CTS
	jnz	cixmx		;
	push	dx
        push    cx
	add	dl,5
	in	al,dx
	test	al,20h
	stc
	jz	cixmitjoin
	test	[dial_flags],-1
	jz	cixmitnodial
	call	dial_xmit_isr
	jmp	cixmitjoin
cixmitnodial:
	call	FetchPacketByte
cixmitjoin:
        pop     cx
	pop	dx
	jc	cixmx
	out	dx,al
ifdef EXLOG
	extrn 	writetx:proc
	call	writetx
endif
        loop    cifflp
        clc
cixmx:
	ret
cixmit	ENDP
cimodem	PROC
	add	dl,6		; point at MSR
	in	al,dx		; get the value
	test	[clflags],NOMODEM_OPTION
	jnz	cimodectsok
	sub	dl,6
	test	al,080h		; DSR & DCD
	jnz	cimodemok	; branch if sox
	jmp	PPPDown		; go shut the layer down if not.
cimodemok:
	test	al,10h   	; Check CTS bit 
	jnz	cimodects	; see if transition to ok
	or	[nocts],1	; else not ok, stop
	ret
cimodects:
	test	[nocts],0ffh	; see if was CTS
	jz	cimodectsok	; yes - exit
	mov	[nocts],0	; mark it in progress
	push	dx		; and get ready to start transmission again
	call	FetchPacketByte	; get a byte
	pop	dx
	jc	cimodectsok	; get out if no data to send
	out	dx,al
cimodectsok:
	ret
cimodem ENDP
	

commbye:
	iret
commint	PROC	far
	test	cs:[commflag],1
	jnz	commbye
	inc	cs:[commflag]
	push	ds
	push	es
	push	ax
	push	cx
	push	dx
	push	bx
	push	bp
	push	si
	push	di
	mov	ax,cs
	mov	ds,ax
	mov	es,ax
	sti
	cld
cit:
	mov	dx,[commport]	; get the comm port
	add	dl,2		; point to interrupt request reg
	in	al,dx		; get the byte
	sub	dl,2		; DX back at data
	test	al,1
	jnz	ciend
	and	al,6		; get rid of extended bits...
	cmp	al,0
	jz	modemchg
	cmp	al,4		; else check for receive int
	jz	rcv		; yes do it
				; ignore xmit ints here
     	jmp	cit		; loop for more
modemchg:
	call	cimodem
	jmp	cit
rcv:
	call	circv		; call receive subroutine
	jmp	cit
ciend:
	cli
ifndef PORTFOLIO
	push	dx
	mov	dl,[picnum]	; now get the PIC we are to clear
	dec	dx		; point at command port
	sub	dh,dh		;
	mov	al,[speoi]	; do a specific end of interrupt
	out	dx,al
	cmp	dl,0a0h		; see if was second pic
	jnz	cix		; no we can exit
	mov	al,62h		; else do a specific eoi of the first pic as well
	out	20h,al		;
cix:
	pop	dx
endif
	dec	cs:[commflag]
	test	[receiveddispatch],TRANSMIT_HANDLING	; see if transmitting
	jnz	cinoxmit
	or	[receiveddispatch],TRANSMIT_HANDLING	; no, mark so
	sti				; allow ints
	call	cixmit			; schedule xmit
	cli				; clear ints
	and	[receiveddispatch],NOT TRANSMIT_HANDLING; mark xmit done
cinoxmit:
endint_join:
	cli
	test	[receiveddispatch],1 + IN_FSM	; see if already dispatching
	jnz	commintx		; yes, exit
	inc	[receiveddispatch]	; else flag it
rcvlp:
	cli
	call	RemoveRecvBuffer		; get a received packet
	jc	rcvx			; exit if no more
	sti				; else let interrupts run during dispatch
	push	si
	mov	cx,[si].bulen		; restore len
	mov	ax,[si].buprot		; load incoming protocol
	lea	si,[si].bubuf		; get buffer
	call	logrecv			; log incoming
	call	lcp_handler		; transfer to LCP handler
					; which will transfer it as necessary
	pop	si			; free the buffer now...
	call	FreeBuffer
	jmp	rcvlp


rcvx:
	dec	[receiveddispatch]
commintx:
	sti
	pop	di		; restore regs
	pop	si
	pop	bp
	pop	bx
	pop	dx
	pop	cx
	pop	ax
	pop	es
	pop	ds
	iret
commint	ENDP
;
; just in case the comm port locks up, kick it now.
;
int8 PROC
	push	ds
	push	es
	push	ax
	push	cx
	push	dx
	push	bx
	push	bp
	push	si
	push	di
	mov	ax,cs
	mov	ds,ax
	mov	es,ax
	cld
	pushf
	call	dword ptr [old8]
	test	[receiveddispatch],TRANSMIT_HANDLING	; see if transmitting
	jnz	i8noxmit
	call	isxmitdone
	jz	i8noxmit
	or	[receiveddispatch],TRANSMIT_HANDLING	; no, mark so
	sti				; allow ints
	call	SendFirstByte		; schedule xmit
	and	[receiveddispatch],NOT TRANSMIT_HANDLING; mark xmit done
i8noxmit:
	test	[dial_flags],0ffh
	jnz	i8_x
	cli
	test	[receiveddispatch],1 + IN_FSM
	jnz	i8_x
	inc	[receiveddispatch]
	sti
	mov	bx,offset cgroup:LCP_struc
	cmp	[bx].sm_state,ST_INITIAL
	jnz	chkop
	call	lcp_init
	jmp	i8_x2
chkop:	
	cmp	[bx].sm_state,ST_OPENED
	jz	nolcp
	mov	ax,1
	call	sm_timed
	cmp	[LCP_struc].sm_state,ST_OPENED
	jnz	i8_x2
nolcp:
        dec     [echoreqtimer]
        jnz     noechoreq
        call    SendEchoReq
        mov     [echoreqtimer],ECHO_PERIOD
noechoreq:
	test	[authenticated],AU_AUTH
	jnz	chkipcp
	mov	ax,1
	call	pap_retry
	jmp	i8_x2
chkipcp:
	mov	bx,offset cgroup:IPCP_struc
	cmp	[bx].sm_state,ST_INITIAL	; this is taken care of elsewhere, just be sure here...
	jnz	chkopipcp
	call	ipcp_init
	jmp	i8_x2
chkopipcp:
	cmp	[bx].sm_state,ST_OPENED
	jz	i8_x2
	mov	ax,1
	call	sm_timed
i8_x2:
	dec	[receiveddispatch]
i8_x:
	jmp	endint_join
int8 ENDP

;
; unhook vectors in use
;
unhookvects PROC
	push	ds
	mov	al,[vector]
	mov	ah,25h
	lds	dx,[oldvector]
	int	21h
	pop	ds
	push	ds
	mov	al,[commvect]
	mov	ah,25h
	lds	dx,[oldcommvect]
	int	21h
	pop	ds
	push	ds
	mov	al,8
	mov	ah,25h
	lds	dx,[old8]
	int	21h
	pop	ds
	ret
unhookvects ENDP
;
; shut down the COMM port interrupts
;
termisr PROC
	mov	dx,[commport] ; get the COMM port
	inc	dx		; point at interrupt enable reg
	sub	al,al		; disable all interrupts
	out	dx,al		;
        inc     dx              ; point at fifo reg
        out     dx,al           ; turn off fifo
        add     dx,2            ; now drop DTR but keep CTS active
        mov     al,2
        out     dx,al
ifdef PORTFOLIO
	call	UninstallPortfolioInterrupt
else
	mov	dl,[picnum]	; get the PIC address
	sub	dh,dh		;
	in	al,dx		; read the interrupt mask
	mov	ah,[picmask]	; now calculate the OFF mask
	not	ah
	or	al,ah		; and add it in
	out	dx,al		; now disable the PIC
endif
ifdef LOGATUNLOAD
        call    logrundown              ; stop logging
endif
	ret
termisr	ENDP
;
; Free all memory used by tsr
;
freemem PROC
	push	es
	mov	ax,cs		; point to beginning
ifdef EXE
	sub	ax,10h
endif
	push	ax
	dec	ax		; now mark the psp's mem as belonging nowhere
	mov	es,ax
	mov	word ptr es:[1],0	;
	pop	ax
	mov	es,ax		; get the PSP back
	mov	ah,49h		; and free it
	int	21h
	pop	es
	ret
freemem ENDP
TSR	ends
;
; end of resident code
;
RESFLAG segment para public 'CODE'

END_RESIDENT	LABEL	BYTE
RESFLAG ends
;
; data for non-resident portion
;
; error messages first
;
CONFIG segment byte public 'CODE'
notthere	db	"No PPP driver loaded",13,10,'$'
sresident	db	"Installed packet driver handler at vector 0x"
vectnum		db	"  ",13,10,'$'
snonresident	db	"Can't connect, driver not going resident",13,10,'$'
;driverloaded    db      "PPP driver already loaded",13,10,'$'
nofreevects	db	"No vectors free",13,10,'$'
usedvect	db	"Vector specified is used",13,10,'$'
nocommport	db	"comm port not available",13,10,'$'
terminated	db	"Driver unloaded",13,10,'$'
vectlist	db	12,11,12,11 ; This is wrong but we are mimicing EPPPD
commlist	dw	03f8h,02f8h,03e8h,02e8h
vectsfound db	0
foundvect	db	0
foundvectlsppp  db      0
freevect	db	0
oldctrlbrk	dd	0
;
; subroutine to print a text string
;
printtext PROC
        mov     ah,9
        int     21h
        ret
ifdef XXXXX
	push	si
	mov	si,dx
ptl:
	lodsb
	or	al,al
	cmp	al,'$'
	jz	ptx
	mov	ah,0eh
	mov	bh,0
	push	si
	int	10h
	pop	si
	jmp	ptl
ptx:
	pop	si
	ret
endif
printtext ENDP
;
; subroutine to get the BIOS feature byte.
get_feature PROC
	mov	ah,0c0h		; first ask BIOS in case is PS2
	int	15h
	jc	lookatrom	; nope, go look in the ROMS
	or	ah,ah		; look in the ROMS if non-zero ret in AH
	jnz	lookatrom
	mov	dx,es:[bx]	; check the first byte of the system info
	cmp	dx,4
	jae	got_features	; greater than 4, go look further at info
lookatrom:
	cmp	byte ptr es:[0fffeh],0fch	; is it an AT?
	jne	identified
	or	[features],TWOPICS		; yes, has 2 pics
	jmp	identified
got_features:
	cmp	byte ptr es:[bx+2],0fch		; else check against various
	je	ps2				; models
	ja	identified
	cmp	byte ptr es:[bx+2],0f8h
	je	ps2
	ja	identified
	cmp	byte ptr es:[bx+2],09ah
	jbe	identified
ps2:
	mov	ah,es:[bx+5]		; and if we get here we have a valid
	mov	[features],ah		; features byte
identified:
	ret
get_feature ENDP
;
; determine which CPU (80286 max we are interested in
;
get_cpu PROC
	mov	cl,33			; 186 will rotate it 32 times ?
	mov	ax,0ffffh
	shl	ax,cl			;186 or better?
	jz	processor_identified	;no.
	mov	[cputype],1
	
	push	sp			; 286 handles stack pushes properly
	pop	ax
	cmp	ax,sp
	jnz	processor_identified
	mov	[cputype],2
processor_identified:
	ret
	
get_cpu ENDP
;
; when we have the COMM port #, get its value from the BIOS table
;
setport PROC
ifdef PORTFOLIO
	mov	bx,03f8h
else
	mov	bl,cs:[commnum]	; port #in BX
	sub	bh,bh
	push	bx
	shl	bl,1		; ports are 2 bytes
	mov	bx,cs:[bx+commlist]	; load port address
endif
	mov	cs:[commport],bx	; into our mem
	pop	bx
	mov	al,cs:[bx+vectlist]
	mov	cs:[commvect],al	; and save it
	ret
setport ENDP
;
; make sure we have a comm port where they specified
;
verifyport PROC
	mov	dx,[commport]	; get it
	or	dx,dx		; zero = error
	jz	vpe
	inc	dx		; else read from the in service reg
	inc	dx		;
	in	al,dx		
	cmp	al,0ffh		; if not FF (will usually be 1)
	jnz	iscom		; then we are ok
vpe:
	mov	dx,offset cgroup:nocommport
	jmp	errexit
iscom:
	ret
verifyport ENDP
;
; check a speculative packet vector for our driver string
;
verifypacketvect	PROC
	sub	ax,ax
	mov	es,ax
	push	bx		; BX = vector
	shl	bx,1		; *4
	shl	bx,1
	les	di,es:[bx]	; load vector (ES is zero)
	add	di,3		; point at sig
	mov	si,offset cgroup:pdmsg	; now load our pointer to what should be there
	mov	cx,8		; compare
	repe	cmpsb
	stc			; get out with carry set if is not there
	jnz	notthisone	
        mov     cx,5            ; check for LSPPP
        inc     si
        inc     di
        repe    cmpsb
        jnz     notlsppp
        pop     ax
        mov     [foundvectlsppp],al
        push    ax
notlsppp:
	mov	di,bx
	clc			; get out with carry clear if is there
notthisone:
	pop	bx
	ret
verifypacketvect	ENDP
;
; Gather statistics about the legal packet space
;
scanvects PROC
ifdef PORTFOLIO
	mov	bx,62h
else
	mov	bx,60h		; 60h = first we will check
				; (yes I know the new standard is more
				;  liberal)
endif
svl:
	call	verifypacketvect; See if any driver here
	jnc	ishere		; branch if so
	test	[freevect],-1	; else if we have got a free vect
	jnz	notfound	; loop
	mov	[freevect],bl   ; else save this as the vector we will use
	jmp	notfound
ishere:
	push	bx
	inc	[vectsfound]	; we found a vect
	sub	bx,bx           ; point at vector tab
	mov	es,bx		
	mov	ax,1ffh		; PACKT function, get driver info
	mov	bx,1		;
	push	ds		; guard against epppd
	push	es
	pushf			; call driver function
	call	dword ptr es:[di]
	pop	es
	pop	ds
	pop	bx
	jc	notfound	; if an older driver, ignore it
	cmp	ch,byte ptr [p_class]	; else see if is PPP driver
	jnz	notfound
	mov	[foundvect],bl	; yes, mark that there is an existing PPP driver
notfound:
	inc	bl		; next packet
	cmp	bl,67h		; skip the EMS vector
	je	notfound
	jc	svl		; loop if less than EMS
	
	cmp	bl,70h		; loop if less than 2nd PIC
	jc	svl
	jne	cmp80		; if above 70h assume we have adjust past PIC
	mov	bl,78h		; else do the adjustment
	jmp	svl
cmp80:
	cmp	bl,80h		; this is the last vector we are checking
	jc	svl
	ret
scanvects ENDP
;
;
loadpacketvect PROC
	call	scanvects	; scan vectors
;        test    [foundvect],-1  ; error if driver loaded
;        jnz     alreadyhere     ;
	test	[vector],-1	; see if user specified a vector
	jz	usefree		; no, use the one suggested
	mov	bl,[vector]	; else see if anything using this one
        sub     bh,bh           ;
	call	verifypacketvect;
	jnc	badspecvect	; error if so
	ret
usefree:
	test	[freevect],-1	; see if any free
	jz	novects		; error if not
	mov	al,[freevect]	; else load it up as the one to use
	mov	[vector],al
lpret:
	ret
badspecvect:
	mov	dx,offset cgroup:usedvect
	jmp	errexit
novects:
	mov	dx,offset cgroup:nofreevects
	jmp	errexit
alreadyhere:
;        mov     dx,offset cgroup:driverloaded
;        jmp     errexit
	
loadpacketvect ENDP
;
; hook the two vectors we need (COMM & Packet Driver)
;
hookvects PROC
	mov	al,[commvect]   ; get the interrupts into local mem
	mov	ah,35h
	int	21h
	mov	word ptr [oldcommvect],bx
	mov	word ptr [oldcommvect+2],es
	mov	al,[vector]   ; 
	mov	ah,35h
	int	21h
	mov	word ptr [oldvector],bx
	mov	word ptr [oldvector+2],es
	mov	ax,3508h
	int	21h
	mov	word ptr [old8],bx
	mov	word ptr [old8+2],es
	

	mov	al,[vector]	; save our values for the vectors
	mov	ah,25h
	mov	dx,offset cgroup:packetint
	int	21h
	mov	al,[commvect]
	mov	ah,25h
	mov	dx,offset cgroup:commint
	int	21h
	mov	ax,2508h
	mov	dx,offset cgroup:int8
	int	21h
	ret
hookvects ENDP
;
; initialize the COMM port
;
initcommport PROC
	cli

dobaudrate:
	mov	ax,49664	; DX:AX = 115200
	mov	dx,1
	div	[baudrate]	; AX = UART value for baudrate
				; we have minimized baud rate at 100
				; so there should be no divide error
	mov	cx,ax		; CX = UART value for baudrate
	mov	dx,[commport]	; DX = comm port
	
	add	dl,3		; point to config reg
	mov	al,83h		; enable the baud rate regs
	out	dx,al		;
	sub	dl,3		; back at first baud rate reg
	jmp	$+2
	mov	al,cl		; put in the LSB
	out	dx,al
	inc	dx		; second baud rate reg
	jmp	$+2
	mov	al,ch		; put in the MSB
	out 	dx,al

nobaudrate:
	mov	dx,[commport]
	add	dl,3		; point back at the config reg
	mov	al,3		; now disable baud rate regs & set 8 bit no parity
	out	dx,al		
	inc	dx   		; point at MCR
	jmp	$+2
	mov	al,0bh		; set lines and OUT2
	out	dx,al
	sub	dl,3		; point at interrupt enable reg
	mov	al,0bh		; enable xmit and receive and modem interrupts
	out	dx,al

	inc	dx		; fifo control reg/ in service reg
	mov	al,7 		; clear the fifos
	out	dx,al
	mov	al,[fifo_setting] ; turn the fifo on with the requested trigger level
        or      al,al
        jz      clilp
	out	dx,al
	jmp	$+2
	jmp	$+2
	in	al,dx		; see if is buggy fifo
	and	al,0c0h		; upper two bits must be 1
	xor	al,0c0h
	jz	okfifo
	sub	al,al		; nope, it is a buggy fifo
	out	dx,al
	jmp	$+2
	jmp	$+2
okfifo:
        mov     [fifo_on],1
				; we are going to make sure interrupts
				; are cleared out in case this is right
				; after powerup\
clilp:
	in	al,dx		; Get the in-service value
	and	al,7		;
	cmp	al,2		; Transmit?
	jz	clilp		; yes- ignore it
	cmp	al,4		; receive?
	jnz	chk0		; no - check modem
	sub	dl,2		; yes, discard the byte
	in	al,dx		;
	add	dl,2		;
	jmp	clilp		;
chk0:
	or	al,al		; Modem?
	jnz	noint		; no, assume done
	add	dl,4		; yes - read the modem status reg
	in	al,dx
	sub	dl,4
	jmp	clilp
noint:
ifdef PORTFOLIO
	call	InstallPortfolioInterrupt
else
	mov	cl,[commvect]	; get vector
	and	cl,7
	mov	al,1		; now calculate the interrupt mask and PIC address
	shl	al,cl		; mask in AL or AH
	xchg	ax,cx		; now in CL or CH
	or	al,60h		; make specific eoi value out of cl
	mov	[speoi],al
	not	cl		; make it the enable mask
	test	[commvect],8		; see if second pic
	jnz	sfirstport	; no, use first
	in	al,0a1h		; else mask in the interrupt
	jmp	$+2
	jmp	$+2
	and	al,cl
	out	0a1h,al
	mov	[picmask],cl 	; save the mask
	mov	[picnum],0a1h	; and the pic address
	jmp	termst
sfirstport:
	in	al,021h		; first pC
	jmp	$+2
	jmp	$+2
	and	al,cl		; mask in the interrupt
	out	021h,al     	;
	mov	[picmask],cl	; save the mask
	mov	[picnum],021h	; and the address
endif
termst:
	sti
	ret
initcommport ENDP
;
; unhook the vectors again
;
terminate PROC
	test	[vector],-1	; see if they specified a vector
	jz	dostats		; no, try to locate one
	mov	bl,[vector]	; else check if the vector is ok
	call	VerifyPacketVect;
	jc	uhve		; no, error
	sub	bx,bx		; point at vector table
	mov	es,bx
	mov	ax,1ffh		; now call it with the driver info func
	mov	bx,1
	push	ds		; guard against epppd
	push	es
	pushf
	call	dword ptr es:[di]
	pop	es
	pop	ds
	jc	uhve		; old driver, ignore it
	cmp	ch,byte ptr [p_class]; is it a PPP driver
	jnz	uhve		; no ignore it
				; else ASSUME it is us for now...
	mov	bl,[vector]
	jmp	uhfound
dostats:
	call	scanvects		; gather statistics
        mov     bl,[foundvectlsppp]     ; find us?
	or	bl,bl
	jnz	uhfound			; yess, go call the unloader
uhve:
	mov	dx,offset cgroup:notthere
	jmp	errexit
	
uhfound:
	sub	ax,ax		; ES = int table
	mov	es,ax
	shl	bx,1		; BX = int offset
	shl	bx,1
	mov	ah,5		; function 5, terminate
	pushf			; go do it
	call	dword ptr es:[bx]
	ret
terminate	ENDP
;
; ctrlbrk ISR
;
ctrlbrkisr PROC
	mov	cs:[ctrlbrk],1
	iret
ctrlbrkisr ENDP
;
; hook ctrl-break vector
;
hookctrlbrk proc
	mov	ax,351bh
	int	21h
	mov	word ptr [oldctrlbrk],bx
	mov	word ptr [oldctrlbrk+2],es
	mov	ax,251bh
	mov	dx,offset cgroup:ctrlbrkisr
	int	21h
	ret
hookctrlbrk endp
;
; unhook ctrl-break vector
;
unhookctrlbrk proc
	push	ds
	mov	ax,251bh
	lds	dx,[oldctrlbrk]
	int	21h
	pop	ds
	ret
unhookctrlbrk endp
;
; close any file handles that may have been redirected
;
closehandles proc
	mov	ah,3eh
	mov	bx,0
	int	21h
	mov	ah,3eh
	mov	bx,1
	int	21h
	mov	ah,3eh
	mov	bx,2
	int	21h
	ret
closehandles endp
;
; main routine
;
go	PROC
ifdef EXE
        mov     bx,ss
        mov     ax,es
        sub     bx,ax
        mov     ax,sp
        add     ax,0fh
        shr     ax,4
        add     bx,ax
else
        mov     bx,1000h
endif
        mov     ah,4ah                  ; make sure there is memory
        int     21h   
ifdef EXE
	mov ax,cs
	mov ds,ax
	mov es,ax
endif
	mov	dx,offset cgroup:banner	; say hi
	call	printtext		;
ifdef	PORTFOLIO
	call	CheckForPortfolio
endif
	call	get_feature		; get features byte
	call	get_cpu			; get CPU type
	call	setport			; set inital comm port and int
	call	parse_comline
	jc	nocom
	test	[clflags],U_OPTION	; see if unload option
	jnz	unload		; yes to do it
	call	pkt_init		; initialize the packet driver routines
	call	loadpacketvect		; else find the packet vectoir to use
	call	hookvects		; hook the vectors
	call	initcommport		; start the COMM port
	push	ds
	pop	es
;	call	hookctrlbrk
	call	dialer			; dial
	jc	noconnect2
;        call    loginit                 ; start logging
	mov	al,[numservers] 	; tell how many servers we default at
	mov	[baseservers],al
	call	connect  		; now connect
ifndef LOGATUNLOAD
	pushf
        call    logrundown              ; stop logging
	popf
endif
	jc	noconnect
;	call	unhookctrlbrk ;
	mov	al,[vector]
	mov	di,offset cgroup:vectnum
	call	formbyte2
	mov	dx,offset cgroup:sresident
	call	printtext
	call	writeconfig		; write ip-up.bat
	call	closehandles
ifdef EXE
	mov	ax,cs
	sub	ax,10h
	mov	es,ax
	mov	es,es:[environ]
else
	mov	es,cs:[environ]		; free our environ, we don't need it
endif
	mov	ah,49h
	int	21h
	mov	ax,offset cgroup:END_RESIDENT +15 ; calculate residency
	mov	cl,4
	shr	ax,cl
	mov	dx,ax
ifdef EXE
	add	dx,10h
endif
	mov	ax,3100h		; go resident
	int	21h
noconnect:
	mov	dx,offset cgroup:snonresident
	call	printtext
	call	hangup			; hang up the modem
noconnect2:
	call	termisr
	call	unhookvects
;	call	unhookctrlbrk
nocom:
	mov	ax,4c01h
	int	21h
;
; unload comes here
;
unload:
	call pkt_init
	call terminate 			; send a term command to them...
	mov	dx,offset cgroup:terminated	; tell them it is gone
	call	printtext
	mov	ax,4c00h		; exit, no errors
	int	21h
go	endp
;
; exit with an error
;
errexit PROC
	call	printtext		; DX = text to print;
	mov	ax,4c01h		; exit with an error
	int	21h
errexit ENDP
config ENDS
	end	start